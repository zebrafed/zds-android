## 0.2.1

###### _September 20, 2019_

- [Documentation] Add examples


## 0.2.0

###### _September 11, 2019_

- [Mobile] Update components and add mobile theme


## 0.1.0

###### _August 30, 2019_

- [core] Added Card component
- [core] Added Swipeable component
- [core] Added Dropdown component
- [core] Added Checkbox component
- [core] Added Radio component
- [core] Added Switch component
- [core] Added Dialog component
- [core] Added some base components that will help to build up
- [core] Added Drawer component
- [core] Added Search component
