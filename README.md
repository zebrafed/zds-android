# Zebra Design System: Android Components

Put copy here about Android components
Hi. My name is Luke Walton. When I was  7 years old I had a dream that one day I would develop some andorid components with a specific look and feel designed by our world class UX designers at Zebra Technologies. This package is the result of 15 years of blood sweat and tears. Please use it wisely. 

## Get Started

Add the library to the app level `build.gradle`

```
dependencies {
    ...
    implementation 'com.zebra.design:Components:0.0.2'
    ...
}
```

This library is hosted by jcenter, so ensure that jcenter() is included in the project level `build.gradle`

```
allprojects {
    repositories {
        ...
        jcenter()
        ...
    }
}
```

This library is built with theme in mind. You must extend our theme in `sytles.xml`.

```
<resources>
    <!-- Base application theme. -->
    <style name="AppTheme" parent="Theme.ZebraMobile">
        ...
    </style>
</resources>
```

Note: this library only works with apps targeting Android API 24 (Nougat) and higer

## Use the library

