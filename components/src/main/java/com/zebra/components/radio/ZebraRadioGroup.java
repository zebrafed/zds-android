package com.zebra.components.radio;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.autofill.AutofillManager;
import android.widget.RadioGroup;

import androidx.annotation.IdRes;

import com.zebra.components.checkbox.ZebraFormControl;

public class ZebraRadioGroup extends RadioGroup {
  private boolean mProtectFromCheckedChange;
  private int mCheckedId = -1;
  private OnCheckedChangeListener mOnCheckedChangeListener;
  private ZebraFormControl.OnCheckedChangeListener mChildOnCheckedChangeListener;
  private PassThroughHierarchyChangeListener mPassThroughListener;

  public ZebraRadioGroup(Context context) {
    this(context, null);
  }

  public ZebraRadioGroup(Context context, AttributeSet attrs) {
    super(context, attrs);

    init();
  }

  private void init() {
    mChildOnCheckedChangeListener = new CheckedStateTracker();
    mPassThroughListener = new PassThroughHierarchyChangeListener();
    super.setOnHierarchyChangeListener(mPassThroughListener);
  }

  @Override
  public void setOnHierarchyChangeListener(OnHierarchyChangeListener listener) {
    // the user listener is delegated to our pass-through listener
    mPassThroughListener.mOnHierarchyChangeListener = listener;
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    if (child instanceof ZebraFormControl) {
      final ZebraFormControl control = (ZebraFormControl) child;
      if (control.isChecked()) {
        mProtectFromCheckedChange = true;
        if (mCheckedId != -1) {
          setCheckedStateForView(mCheckedId, false);
        }
        mProtectFromCheckedChange = false;
        setCheckedId(control.getId());
      }
    }
    super.addView(child, index, params);
  }

  private void setCheckedId(@IdRes int id) {
    boolean changed = id != mCheckedId;
    mCheckedId = id;

    if (mOnCheckedChangeListener != null) {
      mOnCheckedChangeListener.onCheckedChanged(this, mCheckedId);
    }

    if (changed) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        final AutofillManager afm = getContext().getSystemService(AutofillManager.class);
        if (afm != null) {
          afm.notifyValueChanged(this);
        }
      }
    }
  }

  private void setCheckedStateForView(int viewId, boolean checked) {
    View checkedView = findViewById(viewId);
    if (checkedView != null && checkedView instanceof ZebraFormControl) {
      ((ZebraFormControl) checkedView).setChecked(checked);
    }
  }

  @IdRes
  public int getCheckedRadioButtonId() {
    return mCheckedId;
  }

  public void clearCheck() {
    check(-1);
  }

  public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
    mOnCheckedChangeListener = listener;
  }

  public interface OnCheckedChangeListener {
    void onCheckedChanged(ZebraRadioGroup group, @IdRes int checkedId);
  }

  private class CheckedStateTracker implements ZebraFormControl.OnCheckedChangeListener {
    @Override
    public void onCheckedChanged(ZebraFormControl view, boolean isChecked) {
      // prevents from infinite recursion
      if (mProtectFromCheckedChange) {
        return;
      }

      mProtectFromCheckedChange = true;
      if (mCheckedId != -1) {
        setCheckedStateForView(mCheckedId, false);
      }
      mProtectFromCheckedChange = false;

      int id = view.getId();
      setCheckedId(id);
    }
  }

  private class PassThroughHierarchyChangeListener implements
      ViewGroup.OnHierarchyChangeListener {
    private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;

    @Override
    public void onChildViewAdded(View parent, View child) {
      if (parent == ZebraRadioGroup.this && child instanceof ZebraFormControl) {
        int id = child.getId();
        // generates an id if it's missing
        if (id == View.NO_ID) {
          id = View.generateViewId();
          child.setId(id);
        }
        ((ZebraFormControl) child).setOnCheckedChangeListener(mChildOnCheckedChangeListener);
      }

      if (mOnHierarchyChangeListener != null) {
        mOnHierarchyChangeListener.onChildViewAdded(parent, child);
      }
    }

    @Override
    public void onChildViewRemoved(View parent, View child) {
      if (parent == ZebraRadioGroup.this && child instanceof ZebraFormControl) {
        ((ZebraFormControl) child).setOnCheckedChangeListener(null);
      }

      if (mOnHierarchyChangeListener != null) {
        mOnHierarchyChangeListener.onChildViewRemoved(parent, child);
      }
    }
  }
}
