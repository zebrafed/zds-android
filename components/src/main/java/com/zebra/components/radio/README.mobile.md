# Mobile grouped Radio Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
 <com.zebra.Zebra.radio.ZebraRadioGroup
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:id="@+id/checkbox11"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Female">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:button="@drawable/radiobutton_background"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:id="@+id/checkbox12"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Male">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:id="@+id/checkbox13"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Other">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

</com.zebra.Zebra.radio.ZebraRadioGroup>
```