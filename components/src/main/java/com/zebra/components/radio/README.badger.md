# Default Radio Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Radio button unchecked">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:button="@drawable/radiobutton_background"
            android:checked="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Radio button selected">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Radio button disabled">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Radio button disabled">

        <com.zebra.Zebra.radio.ZebraRadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

</LinearLayout>
```

# Grouped Radio Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.radio.ZebraRadioGroup
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.zebra.Zebra.checkbox.ZebraFormControl
            android:id="@+id/checkbox1"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="Female">

            <com.zebra.Zebra.radio.ZebraRadioButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:button="@drawable/radiobutton_background"
                android:checked="true" />
        </com.zebra.Zebra.checkbox.ZebraFormControl>

        <com.zebra.Zebra.checkbox.ZebraFormControl
            android:id="@+id/checkbox2"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="Male">

            <com.zebra.Zebra.radio.ZebraRadioButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content" />
        </com.zebra.Zebra.checkbox.ZebraFormControl>

        <com.zebra.Zebra.checkbox.ZebraFormControl
            android:id="@+id/checkbox3"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="Other">

            <com.zebra.Zebra.radio.ZebraRadioButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content" />
        </com.zebra.Zebra.checkbox.ZebraFormControl>

    </com.zebra.Zebra.radio.ZebraRadioGroup>

</LinearLayout>
```
