package com.zebra.components.radio;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraRadioButton extends AppCompatRadioButton {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_RadioButton;

  public ZebraRadioButton(@NonNull Context context) {
    this(context, null);
  }

  public ZebraRadioButton(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraRadioButton);
  }

  public ZebraRadioButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    context = getContext();

    TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraRadioButton, defStyleAttr, DEF_STYLE_RES);

    attributes.recycle();
  }
}
