package com.zebra.components.input;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraInput extends AppCompatEditText {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Input;
    private static final String TAG = "com.zebra.Zebra.input.ZebraInput";
    private Drawable mIcon;
    private boolean hasDelete;

    public ZebraInput(@NonNull Context context) {
        this(context, null);
    }

    public ZebraInput(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraInput);
    }

    public ZebraInput(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraInput, defStyleAttr, DEF_STYLE_RES);
        if (Mini(context)) {
            hasDelete = attributes.getBoolean(R.styleable.ZebraInput_hasDeleteButton, true);
        } else {
            hasDelete = attributes.getBoolean(R.styleable.ZebraInput_hasDeleteButton, false);
        }
        mIcon = attributes.getDrawable(R.styleable.ZebraInput_android_drawableEnd);
        attributes.recycle();
        setDelete();
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        setDelete();
    }

    private void setDelete() {
        if (getText() != null) {
            if (hasDelete && getText().length() >= 1) {
                addDeleteButton();
            } else {
                hideDeleteButton();
            }
        }
    }

    private void addDeleteButton() {
        this.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, mIcon, null);
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getX() >= (getWidth() - mIcon.getIntrinsicWidth())) {
                        setText("");
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void hideDeleteButton() {
        setCompoundDrawables(null, null, null, null);
    }
}