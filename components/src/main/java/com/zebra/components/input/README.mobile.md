# Text fields

Text fields allow users to enter text into a UI. They typically appear in forms and dialogs.

# Text field with label example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:input_label="Input label:">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Text field with hint example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="Hint text"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Text field with input label and hint example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="Hint text"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Disabled text field example
```xml
    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:enabled="false"/>
```

# Disabled text field with label and hint example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:input_label="Input label:">


    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="Hint text"
        android:enabled="false"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Text field with error example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:errorMessage="Enter a valid phone number:"
    app:input_label="Phone number:">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="01234567890"
        android:inputType="phone" />
</com.zebra.components.input.ZebraTextInputLayout>

```
Formatting a box that displays an error (Using a REGEX)
```java
ZebraTextInputLayout error = view.findViewById(R.id.textinput_error);
error.setErrorType(ZebraTextInputLayout.EMAIL);
```

# Text field with delete button example
```xml
<com.zebra.components.input.ZebraInput
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:hint="Hint text"
    app:hasDeleteButton="true"/>
```