# Text fields
Text fields allow users to enter text into a UI. They typically appear in forms and dialogs.

# Mini input field example
```xml
<com.zebra.components.input.ZebraInput
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:background="@drawable/input_highlight"
    android:text="@string/with_input_text_highlighted" />
```

# Mini input with hint example
```xml
<com.zebra.components.input.ZebraInput
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:background="@drawable/input_highlight"
    android:text="@string/with_input_text_highlighted"
    android:hint="@string/hint_text" />
```

# Mini input with label example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:input_label="@string/title"
    android:layout_margin="8dp">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Mini input with label and hint example
```xml
<com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:input_label="@string/title"
    android:layout_margin="8dp">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="@string/hint_text"/>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Mini input with buttons example
```xml
 <com.zebra.components.input.ZebraTextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:background="@drawable/input_background"
    app:input_label="@string/title">

    <com.zebra.components.input.ZebraInput
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@drawable/input_highlight"
        android:text="@string/with_input_text_highlighted" />

    <com.zebra.components.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_margin="0dp">

        <com.zebra.components.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:gravity="end|center_vertical"
            android:text="@string/cancel" />

        <com.zebra.components.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:text="@string/ok" />
    </com.zebra.components.card.ZebraCardActions>
</com.zebra.components.input.ZebraTextInputLayout>
```

# Mini input without delete button

```xml
<com.zebra.components.input.ZebraInput
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="@drawable/input_highlight"
    android:text="@string/with_input_text_highlighted"
    app:hasDeleteButton="false"/>
```