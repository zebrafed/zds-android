package com.zebra.components.input;

import android.app.Service;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zebra.components.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;

public class ZebraTextInputLayout extends LinearLayout {
    //TODO check these REGEXs
    public static final String EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String PHONE_NUMBER = "\\d{10}";
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Text_Input_Layout;
    private static final String TAG = "com.zebra.Zebra.input.ZebraTextInputLayout";
    private int labelColor, backgroundColor, padding, lineWidth, errorColor;
    private boolean isError;
    private String label, errorLabel, curVal;
    private Pattern pattern;
    private GradientDrawable border;
    private TextView labelTextView;
    private List<ZebraInput> zebraInputs;
    private Handler mHandler = new Handler();


    public ZebraTextInputLayout(Context context) {
        this(context, null);
    }

    public ZebraTextInputLayout(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraTextInputLayout);
    }

    public ZebraTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraTextInputLayout, defStyleAttr, DEF_STYLE_RES);

        //Get attrtibutes
        label = attributes.getString(R.styleable.ZebraTextInputLayout_input_label);
        labelColor = attributes.getColor(R.styleable.ZebraTextInputLayout_labelColor, context.getColor(R.color.primaryColor));
        backgroundColor = attributes.getColor(R.styleable.ZebraTextInputLayout_android_colorBackground, context.getColor(R.color.white));
        padding = attributes.getDimensionPixelSize(R.styleable.ZebraTextInputLayout_android_padding, px(8, context));
        lineWidth = attributes.getDimensionPixelSize(R.styleable.ZebraTextInputLayout_lineWidth, px(1, context));
        errorColor = attributes.getColor(R.styleable.ZebraTextInputLayout_colorError, context.getColor(R.color.red));
        errorLabel = attributes.getString(R.styleable.ZebraTextInputLayout_errorMessage);
        zebraInputs = new ArrayList<>();
        this.setOrientation(VERTICAL);

        //Set label
        if (label != null) {
            setOrientation(LinearLayout.VERTICAL);
            labelTextView = new TextView(context, attrs);
            labelTextView.setTextAppearance(R.style.Input_Label_Text_Appearance);
            labelTextView.setPadding(padding, padding, padding, 0);
            labelTextView.setText(label);
            labelTextView.setTextColor(labelColor);
            labelTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            addView(labelTextView, 0);
        }

        //Set background
        border = new GradientDrawable();
        border.setColor(backgroundColor);
        border.setStroke(lineWidth, backgroundColor);
        setPadding(lineWidth, lineWidth, lineWidth, lineWidth);
        setBackground(border);

        attributes.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int l, final int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        //If multiple Inputs are inside a single layout
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) instanceof ZebraInput) {
                ZebraInput zebraInput = (ZebraInput) getChildAt(i);
                zebraInputs.add(zebraInput);
                zebraInput.setBackground(null);
                setEnabled(zebraInput.isEnabled());

                zebraInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        //Regular border if typing
                        setError(false);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        //Regular border if typing
                        setError(false);
                    }

                    @Override
                    public void afterTextChanged(final Editable s) {
                        //Sets the error border if user stops typing for 1 second.
                        curVal = s.toString();
                        mHandler.removeCallbacks(mFilterTask);
                        mHandler.postDelayed(mFilterTask, 1000);
                    }
                });
                zebraInput.setOnFocusChangeListener(new OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        onFocusDraw(hasFocus);
                    }
                });
            }
        }

        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                selectTextView();
            }
        });

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTextView();
            }
        });
    }

    private Runnable mFilterTask = new Runnable() {
        @Override
        public void run() {
            applyErrorStyle();
        }
    };

    private void selectTextView() {
        //Sets focus to the input, rather than this layout
        if (zebraInputs.get(0).isEnabled()) {
            zebraInputs.get(0).requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.showSoftInput(zebraInputs.get(0), 0);
        }
    }

    private void applyErrorStyle() {
        if (pattern != null) {
            setError(!(pattern.matcher(curVal).matches()));
        } else {
            setError(false);
        }
    }

    private void applyLabelColor(int col) {
        if (labelTextView != null) labelTextView.setTextColor(col);
    }

    private void applyBorderColor(int col) {
        border.setStroke(lineWidth, col);
        setBackground(border);
    }

    private void onFocusDraw(boolean focus) {
        if (focus) {
            applyBorderColor(labelColor);
            applyLabelColor(labelColor);

            if (isError) {
                applyBorderColor(errorColor);
                applyLabelColor(errorColor);
                if (labelTextView != null) labelTextView.setText(errorLabel);
            } else {
                if (labelTextView != null) labelTextView.setText(label);
            }
        } else {
            applyBorderColor(backgroundColor);
            applyLabelColor(labelColor);
            if (labelTextView != null) labelTextView.setText(label);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setFocusable(enabled);
        if (enabled) {
            setBackground(border);
            applyLabelColor(labelColor);
        } else {
            setBackgroundColor(getContext().getColor(R.color.gray_300));
            applyLabelColor(getContext().getColor(R.color.gray_900));
        }
    }

    public void setInputLabelColor(int inputColor) {
        this.labelColor = inputColor;
        applyLabelColor(inputColor);
    }

    public void setError(boolean bool) {
        isError = bool;
        onFocusDraw(true);
    }

    public void setErrorType(String regex) {
        pattern = Pattern.compile(regex);
    }
}
