package com.zebra.components.timepicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zebra.components.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraTimePicker extends LinearLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_TimePicker;
    private static final String TAG = "com.zebra.components.timepicker.ZebraTimePicker";
    private static int REP_DELAY = 50;

    private boolean mAutoIncrement, mAutoDecrement, hAutoIncrement, hAutoDecrement, enabled;
    private int minute, hour, color, disabledColor;
    private TextView timeView, minuteView, hourView, colonView;
    private ImageButton hourPlus, hourMinus, minPlus, minMinus;
    private Handler mrepeatUpdateHandler = new Handler();
    private Handler hrepeatUpdateHandler = new Handler();

    public ZebraTimePicker(@NonNull Context context) {
        this(context, null);
    }

    public ZebraTimePicker(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraTimePicker);
    }

    public ZebraTimePicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        View theView = inflate(context, R.layout.time_picker, this);

        timeView = theView.findViewById(R.id.timeView);
        minuteView = theView.findViewById(R.id.minutes);
        hourView = theView.findViewById(R.id.hours);
        colonView = theView.findViewById(R.id.colon);
        hourPlus = theView.findViewById(R.id.hourPlus);
        hourMinus = theView.findViewById(R.id.hourMinus);
        minPlus = theView.findViewById(R.id.minPlus);
        minMinus = theView.findViewById(R.id.minMinus);

        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);

        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraTimePicker, defStyleAttr, DEF_STYLE_RES);
        setHour(attributes.getInteger(R.styleable.ZebraTimePicker_hour, calendar.get(Calendar.HOUR_OF_DAY)));
        setMinute(attributes.getInteger(R.styleable.ZebraTimePicker_minute, calendar.get(Calendar.MINUTE)));
        color = attributes.getColor(R.styleable.ZebraTimePicker_android_color, context.getColor(R.color.primaryColor));
        disabledColor = attributes.getColor(R.styleable.ZebraTimePicker_colorDisabled, context.getColor(R.color.gray_300));
        setEnabled(attributes.getBoolean(R.styleable.ZebraTimePicker_enabled, true));
        attributes.recycle();

        setHourInc();
        setHourDec();
        setMinuteDec();
        setMinuteInc();
    }

    private void setHourInc() {
        hourPlus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                hAutoIncrement = true;
                hrepeatUpdateHandler.post(new hRptUpdater());
                return false;
            }
        });


        hourPlus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setHour(hour + 1);
                }
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && hAutoIncrement) {
                    hAutoIncrement = false;
                }
                return false;
            }
        });
    }

    private void setHourDec() {
        hourMinus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                hAutoDecrement = true;
                hrepeatUpdateHandler.post(new hRptUpdater());
                return false;
            }
        });


        hourMinus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setHour(hour - 1);
                } else if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && hAutoDecrement) {
                    hAutoDecrement = false;
                }
                return false;
            }
        });
    }

    private void setMinuteInc() {
        minPlus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoIncrement = true;
                mrepeatUpdateHandler.post(new mRptUpdater());
                return false;
            }
        });


        minPlus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setMinute(minute + 1);
                }
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoIncrement) {
                    mAutoIncrement = false;
                }
                return false;
            }
        });
    }

    private void setMinuteDec() {
        minMinus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoDecrement = true;
                mrepeatUpdateHandler.post(new mRptUpdater());
                return false;
            }
        });


        minMinus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setMinute(minute - 1);
                }
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoDecrement) {
                    mAutoDecrement = false;
                }
                return false;
            }
        });
    }

    private class mRptUpdater implements Runnable {
        public void run() {
            if (mAutoIncrement) {
                setMinute(minute + 1);
                mrepeatUpdateHandler.postDelayed(new mRptUpdater(), REP_DELAY);
            } else if (mAutoDecrement) {
                setMinute(minute - 1);

                mrepeatUpdateHandler.postDelayed(new mRptUpdater(), REP_DELAY);
            }
        }
    }

    private class hRptUpdater implements Runnable {
        public void run() {
            if (hAutoIncrement) {
                setHour(hour + 1);

                hrepeatUpdateHandler.postDelayed(new hRptUpdater(), REP_DELAY);
            } else if (hAutoDecrement) {
                setHour(hour - 1);

                hrepeatUpdateHandler.postDelayed(new hRptUpdater(), REP_DELAY);
            }
        }
    }

    private void refreshTimeView() {
        String hourS = Integer.toString(hour);
        if (hour < 10) hourS = "0" + hourS;

        String minS = Integer.toString(minute);
        if (minute < 10) minS = "0" + minS;

        timeView.setText(getContext().getString(R.string.totalTime, hourS, minS));
    }

    public void setMinute(int minute) {
        if (minute > 59) {
            minute = 0;
            setHour(hour + 1);
        } else if (minute < 0) {
            minute = 59;
            setHour(hour - 1);
        }

        this.minute = minute;

        String minuteS = Integer.toString(minute);
        if (minute < 10) minuteS = "0" + minuteS;
        minuteView.setText(minuteS);
        refreshTimeView();

    }

    public void setHour(int hour) {
        if (hour > 23) hour = 0;
        if (hour < 0) hour = 23;
        this.hour = hour;
        String hourS = Integer.toString(hour);
        if (hour < 10) hourS = "0" + hourS;
        hourView.setText(hourS);
        refreshTimeView();
    }

    public int getMinute() {
        return Integer.parseInt(minuteView.getText().toString());
    }

    public int getHour() {
        return hour;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        hourPlus.setEnabled(enabled);
        hourMinus.setEnabled(enabled);
        minPlus.setEnabled(enabled);
        minMinus.setEnabled(enabled);

        if (enabled) {
            timeView.setBackgroundColor(color);
            hourView.setTextColor(color);
            minuteView.setTextColor(color);
            colonView.setTextColor(color);
        } else {
            timeView.setBackgroundColor(disabledColor);
            hourView.setTextColor(disabledColor);
            minuteView.setTextColor(disabledColor);
            colonView.setTextColor(disabledColor);
        }
    }
}
