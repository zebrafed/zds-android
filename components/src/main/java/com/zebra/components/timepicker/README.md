# Time picker example
Time picker enables users to choose a time by the hour and minute.

```xml
 <com.zebra.components.card.ZebraCardView
    android:layout_gravity="center"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">

    <com.zebra.components.timepicker.ZebraTimePicker
        android:layout_width="wrap_content"
        android:id="@+id/timepicker"
        android:enabled="true"
        android:layout_height="wrap_content"
        android:gravity="center_horizontal" />

    <com.zebra.components.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.components.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:gravity="end|center_vertical"
            android:text="cancel" />

        <com.zebra.components.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:text="ok" />
    </com.zebra.components.card.ZebraCardActions>
</com.zebra.components.card.ZebraCardView>
```

Values can also be set in Java
```java
ZebraTimePicker zebraTimePicker = view.findViewById(R.id.timepicker);
zebraTimePicker.setHour(14);
zebraTimePicker.setMinute(57);
```