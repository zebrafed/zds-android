package com.zebra.components.switchcontrol;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraSwitch extends SwitchCompat {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_Switch;

  public ZebraSwitch(@NonNull Context context) {
    this(context, null);
  }

  public ZebraSwitch(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraSwitch);
  }

  public ZebraSwitch(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    context = getContext();

    TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraSwitch, defStyleAttr, DEF_STYLE_RES);

    attributes.recycle();
  }

  @Override
  public void draw(Canvas c) {
    super.draw(c);
    final Drawable background = getBackground();
    if (background != null) {
      DrawableCompat.setHotspotBounds(background, 0, 0, 0, 0);
    }
  }
}