# Mobile Switch examples left aligned

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
 <com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch off"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch on"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch disabled"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch on disabled"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>
```

# Mobile Switch Example withtout border, left aligned

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:text="Switch off"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:text="Switch on"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:text="Switch disabled"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:enabled="false"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="8dp"
    android:text="Switch on disabled"
    app:reverse="true">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:enabled="true"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>
```

# Mobile Switch Example, right aligned

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch off">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch on">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch disabled">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Switch on disabled">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>
```

# Mobile Switch Example more content

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Item">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Item"
    app:description="Details">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Item"
    app:description="Details, in case we need a two line long explanation">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:foreground="@drawable/card_header"
    android:text="Item with a long title that has two lines"
    app:description="Details, in case we need a two line long explanation">

    <com.zebra.Zebra.switchcontrol.ZebraSwitch
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>
```