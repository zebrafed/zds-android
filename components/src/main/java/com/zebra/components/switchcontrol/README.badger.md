# Switch examples left aligned

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
 <LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch off"
        app:reverse="true">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch on"
        app:reverse="true">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch disabled"
        app:reverse="true">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch disabled"
        app:reverse="true">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

</LinearLayout>
```

# Switch examples right aligned

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch off"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch on"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch disabled"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Switch disabled"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

</LinearLayout>
```

# Switch examples more content

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Item"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>


    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Item"
        app:description="Details"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Item"
        app:description="Details, in case we need a two line long explanation"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Item with a long title that has two lines"
        app:description="Details, in case we need a two line long explanation"
        app:reverse="false">

        <com.zebra.Zebra.switchcontrol.ZebraSwitch
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

</LinearLayout>
```