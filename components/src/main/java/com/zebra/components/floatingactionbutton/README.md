#  Floating Action Button Example
A floating action button (FAB) performs the primary, or most common action on a screen. It appears in front of all screen content, as a circular shape with an icon in its center.
Floating Action Buttons work best within CoordinatorLayout as you get certain behaviour's for free. For more information, [see Material Design](https://material.io/develop/android/components/floating-action-button/)

```xml
<androidx.coordinatorlayout.widget.CoordinatorLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content">
    
     <com.zebra.components.floatingactionbutton.ZebraFloatingActionButton
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom|end"
        android:layout_margin="16dp"
        app:icon="@drawable/ic_send" />
</androidx.coordinatorlayout.widget.CoordinatorLayout>

*To use CoordinatorLayout, include the following in your app-level build.gradle*
```
    implementation "androidx.coordinatorlayout:coordinatorlayout:1.1.0"
```