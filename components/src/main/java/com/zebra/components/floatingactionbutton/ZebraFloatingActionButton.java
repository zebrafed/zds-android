package com.zebra.components.floatingactionbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.zebra.components.R;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraFloatingActionButton extends LinearLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_FloatingActionButton;
    private int outerColor, innerColor, iconColor;
    private Drawable outer, inner, icon;
    private LayerDrawable layerDrawable,fabRipple;

    public ZebraFloatingActionButton(@NonNull Context context) {
        this(context, null);
    }

    public ZebraFloatingActionButton(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraFloatingActionButton);
    }

    public ZebraFloatingActionButton(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraFloatingActionButton, defStyleAttr, DEF_STYLE_RES);

        fabRipple = (LayerDrawable) context.getDrawable(R.drawable.fab_ripple);
        assert fabRipple != null;
        layerDrawable = (LayerDrawable) fabRipple.findDrawableByLayerId(R.id.list);
        icon = attributes.getDrawable(R.styleable.ZebraFloatingActionButton_icon);

        outerColor = attributes.getColor(R.styleable.ZebraFloatingActionButton_outerColor, context.getColor(R.color.gray_800));
        innerColor = attributes.getColor(R.styleable.ZebraFloatingActionButton_innerColor, context.getColor(R.color.primaryColor));
        iconColor = attributes.getColor(R.styleable.ZebraFloatingActionButton_iconColor, context.getColor(R.color.white));

        getStyle();
        attributes.recycle();
    }

    private void getStyle() {
        outer = layerDrawable.findDrawableByLayerId(R.id.outer);
        inner = layerDrawable.findDrawableByLayerId(R.id.inner);

        applyStyle();
    }

    private void applyStyle() {
        outer.setTint(outerColor);
        inner.setTint(innerColor);

        if (icon != null) {
            icon.setTint(iconColor);
            layerDrawable.setDrawable(2, icon);
        } else {
            layerDrawable.setDrawable(2, null);
        }

        this.setBackground(fabRipple);
    }

    public Drawable getIcon() {
        return icon;
    }

    public int getOuterColor() {
        return outerColor;
    }

    public int getInnerColor() {
        return innerColor;
    }

    public int getIconColor() {
        return iconColor;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
        applyStyle();
    }

    public void setOuterColor(int outerColor) {
        this.outerColor = outerColor;
        applyStyle();
    }

    public void setInnerColor(int innerColor) {
        this.innerColor = innerColor;
        applyStyle();

    }

    public void setIconColor(int iconColor) {
        this.iconColor = iconColor;
        applyStyle();
    }
}
