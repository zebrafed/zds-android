# Default Checkbox Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_gravity="center_horizontal"
    android:background="@color/backgroundColor"
    android:orientation="vertical"
    android:padding="8dp">

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox unchecked">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox selected">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox indeterminate">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:indeterminate="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox disabled">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="false"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox disabled">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:checked="true"
            android:enabled="false" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>

    <com.zebra.Zebra.checkbox.ZebraFormControl
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Checkbox disabled">

        <com.zebra.Zebra.checkbox.ZebraCheckbox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:enabled="false"
            app:indeterminate="true" />
    </com.zebra.Zebra.checkbox.ZebraFormControl>
</LinearLayout>
```