package com.zebra.components.checkbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.widget.CompoundButtonCompat;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.getColorStateList;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraCheckbox extends AppCompatCheckBox {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Checkbox;
  private static final int[] STATE_INDETERMINATE = {R.attr.state_indeterminate};
  private boolean mIsIndeterminate = false;

  public ZebraCheckbox(@NonNull Context context) {
    this(context, null);
  }

  public ZebraCheckbox(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraCheckbox);
  }

  public ZebraCheckbox(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    context = getContext();

    TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraCheckbox, defStyleAttr, DEF_STYLE_RES);

    if (attributes.hasValue(R.styleable.ZebraCheckbox_tintColor)) {
      CompoundButtonCompat.setButtonTintList(
          this,
          getColorStateList(context, attributes, R.styleable.ZebraCheckbox_tintColor)
      );
    }

    if (attributes.hasValue(R.styleable.ZebraCheckbox_indeterminate)) {
      boolean indeterminate = attributes.getBoolean(R.styleable.ZebraCheckbox_indeterminate, false);
      if (indeterminate) {
        setIndeterminate(true);
      }
    }

    attributes.recycle();
  }

  void setIndeterminate(boolean state) {
    mIsIndeterminate = state;
    refreshDrawableState();
  }

  @Override
  protected int[] onCreateDrawableState(int extraSpace) {
    final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
    if (mIsIndeterminate) {
      mergeDrawableStates(drawableState, STATE_INDETERMINATE);
    }

    return drawableState;
  }

  @Override
  public void setChecked(boolean checked) {
    if (mIsIndeterminate) {
      setIndeterminate(false);
      super.setChecked(true);
      return;
    }
    super.setChecked(checked);
  }
}
