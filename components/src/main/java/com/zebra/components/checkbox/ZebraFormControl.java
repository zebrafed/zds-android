package com.zebra.components.checkbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zebra.components.R;

import static androidx.appcompat.widget.ListPopupWindow.MATCH_PARENT;
import static androidx.appcompat.widget.ListPopupWindow.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraFormControl extends LinearLayout implements View.OnClickListener {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_FormControl;
  private final int mContentPaddingTop;
  private final int mContentPaddingBottom;
  private int mContentPaddingStart = 0;
  private int mContentPaddingEnd = 0;
  private int mLabelTextAppearance;
  private int mDescriptionTextAppearance;
  private TextView mLabel;
  private TextView mDescription;
  private boolean mReverse;
  private CompoundButton mControl;
  private LinearLayout mContentLayout;

  public ZebraFormControl(Context context) {
    this(context, null);
  }

  public ZebraFormControl(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraFormControl);
  }

  public ZebraFormControl(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraFormControl, defStyleAttr, DEF_STYLE_RES);

    LinearLayout.LayoutParams contextLayout = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT, 1);
    mContentLayout = new LinearLayout(getContext());
    mContentLayout.setOrientation(LinearLayout.VERTICAL);
    super.addView(mContentLayout, -1, contextLayout);

    if (attributes.hasValue(R.styleable.ZebraFormControl_contentPaddingStart) || attributes.hasValue(R.styleable.ZebraFormControl_contentPaddingStart)) {
      mContentPaddingStart = attributes.getDimensionPixelSize(R.styleable.ZebraFormControl_contentPaddingStart, 0);
      mContentPaddingEnd = attributes.getDimensionPixelSize(R.styleable.ZebraFormControl_contentPaddingEnd, 0);
    }

    mContentPaddingTop = attributes.getDimensionPixelSize(R.styleable.ZebraFormControl_contentPaddingTop, 0);
    mContentPaddingBottom = attributes.getDimensionPixelSize(R.styleable.ZebraFormControl_contentPaddingBottom, 0);

    if (attributes.hasValue(R.styleable.ZebraFormControl_reverse)) {
      boolean reverse = attributes.getBoolean(R.styleable.ZebraFormControl_reverse, false);
      setReverse(reverse);
    }else {
      setReverse(false);
    }
    if (attributes.hasValue(R.styleable.ZebraFormControl_textAppearance)) {
      mLabelTextAppearance = attributes.getResourceId(R.styleable.ZebraFormControl_textAppearance, R.style.Widget_Zebra_FormControl_TextAppearance);
    }
    if (attributes.hasValue(R.styleable.ZebraFormControl_textAppearance)) {
      mDescriptionTextAppearance = attributes.getResourceId(R.styleable.ZebraFormControl_descriptionTextAppearance, R.style.Widget_Zebra_FormControl_TextAppearance);
    }
    if (attributes.hasValue(R.styleable.ZebraFormControl_android_text)) {
      String label = attributes.getString(R.styleable.ZebraFormControl_android_text);
      setText(label);
    }
    if (attributes.hasValue(R.styleable.ZebraFormControl_description)) {
      String label = attributes.getString(R.styleable.ZebraFormControl_description);
      setDescription(label);
    }

    setOnClickListener(this);

    attributes.recycle();
  }

  @Override
  public void addView(View child) {
    mControl = (CompoundButton) child;
    super.addView(mControl);
  }

  @Override
  public void addView(View child, int index) {
    mControl = (CompoundButton) child;
    super.addView(child, index);
  }

  @Override
  public void addView(View child, int width, int height) {
    mControl = (CompoundButton) child;
    super.addView(child, width, height);
  }

  @Override
  public void addView(View child, ViewGroup.LayoutParams params) {
    mControl = (CompoundButton) child;
    super.addView(child, params);
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    mControl = (CompoundButton) child;
    super.addView(child, mReverse ? 0 : -1, params);
  }

  public void setOnCheckedChangeListener(final OnCheckedChangeListener listener) {
    if (listener == null) {
      mControl.setOnCheckedChangeListener(null);
      return;
    }
    mControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        listener.onCheckedChanged(ZebraFormControl.this, isChecked);
      }
    });
  }

  @Override
  public boolean isEnabled() {
    if (mControl != null) {
      return mControl.isChecked();
    }
    return super.isEnabled();
  }

  public boolean isChecked() {
    if (mControl != null) {
      return mControl.isChecked();
    }
    return false;
  }

  public void setChecked(boolean state) {
    if (mControl != null) {
      if (mControl.isEnabled()) {
        mControl.setChecked(state);
      }
    }
  }

  public void toggle() {
    if (mControl != null) {
      if (mControl.isEnabled()) {
        mControl.toggle();
      }
    }
  }

  void setText(String value) {
    if (mLabel == null) {
      mLabel = new TextView(getContext());
      LinearLayout.LayoutParams textLayout = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

      mLabel.setTextAppearance(getContext(), mLabelTextAppearance);
      mContentLayout.addView(mLabel, -1, textLayout);
    }
    mLabel.setText(value);
  }

  void setDescription(String value) {
    if (mDescription == null) {
      mDescription = new TextView(getContext());
      LinearLayout.LayoutParams textLayout = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

      mDescription.setTextAppearance(getContext(), mDescriptionTextAppearance);
      mContentLayout.addView(mDescription, -1, textLayout);
    }
    mDescription.setText(value);
  }

  void setReverse(boolean state) {
    mReverse = state;
    LinearLayout.LayoutParams contextLayout = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT, 1);
    if (state) {
      contextLayout.setMargins(0, mContentPaddingTop, mContentPaddingStart, mContentPaddingBottom);
    }else {
      contextLayout.setMargins(mContentPaddingStart, mContentPaddingTop, 0, mContentPaddingBottom);
    }

    if (mContentLayout != null) {
      super.removeView(mContentLayout);

      if (mReverse) {
        super.addView(mContentLayout, -1, contextLayout);
      }else {
        super.addView(mContentLayout, 0, contextLayout);
      }
    }
  }

  @Override
  public void onClick(View v) {
    toggle();
  }

  public interface OnCheckedChangeListener {
    void onCheckedChanged(ZebraFormControl view, boolean isChecked);
  }
}
