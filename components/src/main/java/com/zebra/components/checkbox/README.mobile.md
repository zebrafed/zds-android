# Mobile Checkbox Example without border

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox unchecked">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox checked">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="0dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox partially checked">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:indeterminate="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="0dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox disabled">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="false"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox checked disabled">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checked="true"
        android:enabled="false" />
</com.zebra.Zebra.checkbox.ZebraFormControl>

<com.zebra.Zebra.checkbox.ZebraFormControl
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="0dp"
    android:layout_marginEnd="8dp"
    android:text="Checkbox partially checked disabled">

    <com.zebra.Zebra.checkbox.ZebraCheckbox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:enabled="false"
        app:indeterminate="true" />
</com.zebra.Zebra.checkbox.ZebraFormControl>
```