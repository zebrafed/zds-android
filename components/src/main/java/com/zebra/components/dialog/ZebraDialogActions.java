package com.zebra.components.dialog;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zebra.components.R;

import static androidx.appcompat.widget.ListPopupWindow.MATCH_PARENT;
import static androidx.appcompat.widget.ListPopupWindow.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraDialogActions extends LinearLayout {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_DialogActions;
  private final int mWidth;
  private final int mInnerSpace;

  public ZebraDialogActions(@NonNull Context context) {
    this(context, null /* attrs */);
  }

  public ZebraDialogActions(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraDialogActions);
  }

  public ZebraDialogActions(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraCardActions, defStyleAttr, DEF_STYLE_RES);

    int paddingBottom = attributes.getDimensionPixelSize(R.styleable.ZebraCardActions_android_paddingBottom, 0);
    int orientation = attributes.getInteger(R.styleable.ZebraCardActions_android_orientation, LinearLayout.VERTICAL);
    mWidth = attributes.getInteger(R.styleable.ZebraCardActions_itemsLayoutWidth, MATCH_PARENT);
    mInnerSpace = attributes.getDimensionPixelSize(R.styleable.ZebraCardActions_innerSpace, 0);

    setOrientation(orientation);
    setPadding(0, 0, 0, paddingBottom);
    setGravity(Gravity.END);

    attributes.recycle();
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    LayoutParams lp = new LayoutParams(mWidth, WRAP_CONTENT);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    super.addView(child, index, lp);
  }

  @Override
  public void addView(View child) {
    LayoutParams lp = new LayoutParams(mWidth, WRAP_CONTENT);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    child.setLayoutParams(lp);
    super.addView(child);
  }

  @Override
  public void addView(View child, ViewGroup.LayoutParams params) {
    LayoutParams lp = new LayoutParams(mWidth, WRAP_CONTENT);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    super.addView(child, lp);
  }
}
