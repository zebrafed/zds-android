# Dialog with Title, Content and 2 Actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!
​

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!
​
- option 1
- option 2
- option 3

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView"
        style="@style/Widget_Zebra_DialogTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title that may take two lines:" />

    <TextView
        android:id="@+id/textView2"
        style="@style/Widget_Zebra_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore." />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/once"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@android:string/ok" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="@android:string/cancel" />
    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```

```java
final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(240, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            dialog.dismiss();
            }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            dialog.dismiss();
            }
        });
    }
});

dialog.show();
```

# Dialog with Title, Content and three Actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView"
        style="@style/Widget_Zebra_DialogTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title that may take two lines:" />

    <TextView
        android:id="@+id/textView2"
        style="@style/Widget_Zebra_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore." />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/once"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@android:string/ok" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@android:string/cancel" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button_default"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Default" />
    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```

```java
final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(240, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button defaultButton = dialog.findViewById(R.id.button_default);
        defaultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
});

dialog.show();
```

# Dialog with Title, Content, Media and Actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView"
        style="@style/Widget_Zebra_DialogTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Audio Staging help" />

    <TextView
        android:id="@+id/textView2"
        style="@style/Widget_Zebra_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Audio staging is a new beta feature in StageNow tha allows you to stage multiple devices at once, using a single audio file." />

    <ImageView
        android:id="@+id/imageView2"
        android:layout_width="match_parent"
        android:layout_height="104dp"
        android:adjustViewBounds="false"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

    <TextView
        android:id="@+id/textView3"
        style="@style/Widget_Zebra_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingTop="8dp"
        android:text="Ideal audio staging environment"
        android:textSize="12sp" />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Dismiss" />

    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```

```java
final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(240, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.always);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
});

dialog.show();
```

# Bottom Dialog Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:background="@color/whiteColor"
    android:layout_height="wrap_content">

    <TextView
        android:id="@+id/textView"
        style="@style/Widget_Zebra_DialogTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Select a home app" />

    <com.zebra.Zebra.dialog.ZebraDialogItem
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Enterprise Home Screen"
        app:leadingDrawable="@drawable/documents" />

    <com.zebra.Zebra.dialog.ZebraDialogItem
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:leadingDrawable="@drawable/folder"
        android:text="Launcher 3"
        />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/once"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="JUST ONCE" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="ALWAYS" />
    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```

```java
final ZebraBottomSheet dialog = new ZebraBottomSheet(getContext(), R.layout.dialog);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        Button okOnce = dialog.findViewById(R.id.once);
        okOnce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button okAlways = dialog.findViewById(R.id.always);
        okAlways.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
});

dialog.show();
```