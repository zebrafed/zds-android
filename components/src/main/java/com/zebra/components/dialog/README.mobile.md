# Mobile Dialog Example with two actions, content and title

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<!-- R.layout.dialog_mobile_title_simple -->
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView"
        style="@style/Widget_ZebraMobile_DialogTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Card title" />

    <TextView
        android:id="@+id/textView2"
        style="@style/Widget_ZebraMobile_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="40dp"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore." />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/once"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```
```java
final ZebraDialog dialog = new ZebraDialog(this, R.layout.dialog_mobile_title_simple);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
});

dialog.show();
```

# Mobile Dialog Example with two actions without title

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<!-- R.layout.dialog_mobile_simple -->
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView2"
        style="@style/Widget_ZebraMobile_DialogContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="40dp"
        android:paddingTop="18dp"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore." />

    <com.zebra.Zebra.dialog.ZebraDialogActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/once"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/always"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.dialog.ZebraDialogActions>
</LinearLayout>
```
```java
final ZebraDialog dialog = new ZebraDialog(this, R.layout.dialog_mobile_simple);

dialog.setOnShowListener(new DialogInterface.OnShowListener() {
    @Override
    public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
});

dialog.show();
```