package com.zebra.components.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.zebra.components.R;

public class ZebraBottomSheet extends BottomSheetDialog {
  int mContentLayout;

  public ZebraBottomSheet(@NonNull Context context, int layout) {
    super(context, R.style.Widget_Zebra_BottomSheet);

    mContentLayout = layout;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(mContentLayout);
  }
}
