package com.zebra.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.view.Window.FEATURE_NO_TITLE;

public class ZebraDialog extends Dialog {
  int mContentLayout;

  public ZebraDialog(@NonNull Context context, int layout) {
    super(context, 0);

    mContentLayout = layout;
  }

  protected ZebraDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
    super(context, cancelable, cancelListener);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(FEATURE_NO_TITLE);
    setContentView(mContentLayout);
  }
}
