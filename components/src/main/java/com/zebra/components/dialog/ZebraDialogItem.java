package com.zebra.components.dialog;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zebra.components.R;
import com.zebra.components.Utils;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraDialogItem extends LinearLayout implements Checkable {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_DialogItem;

  private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};
  private final int mLeadingDrawableDefaultColor;
  private final int mLeadingDrawableActiveColor;

  private Drawable mLeadingDrawable;
  private ImageView mLeading;
  private String mText;
  private int mTextAppearance;
  private TextView mTextView;
  private boolean mChecked;

  public ZebraDialogItem(Context context) {
    this(context, null);
  }

  public ZebraDialogItem(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraDialogItem);
  }

  public ZebraDialogItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraDialogItem, defStyleAttr, DEF_STYLE_RES);

    mLeadingDrawable = attributes.getDrawable(R.styleable.ZebraDialogItem_leadingDrawable);
    mLeadingDrawableDefaultColor = attributes.getColor(R.styleable.ZebraDialogItem_defaultColor, -1);
    mLeadingDrawableActiveColor = attributes.getColor(R.styleable.ZebraDialogItem_activeColor, -1);
    if (mLeadingDrawable != null) {
      mLeadingDrawable.setBounds( 0, 0, px(24), px(24));
      mLeadingDrawable.setTintList(new ColorStateList(new int[][]{new int[]{}}, new int[]{mLeadingDrawableDefaultColor}));
    }

    setLeadingDrawable(mLeadingDrawable);

    mTextAppearance = attributes.getResourceId(R.styleable.ZebraDialogItem_textAppearance, R.style.Widget_Zebra_DialogItem_TextAppearance);

    if (attributes.hasValue(R.styleable.ZebraDialogItem_android_text)) {
      String text = attributes.getString(R.styleable.ZebraDialogItem_android_text);
      setText(text);
    }

    attributes.recycle();
  }

  public Drawable getLeadingDrawable() {
    return mLeadingDrawable;
  }

  public void setLeadingDrawable(Drawable drawable) {
    LayoutParams lp = new LayoutParams(px(24), px(24));
    lp.setMargins(0,0,px(8),0);

    setLeadingDrawable(drawable, lp);
  }

  public void setLeadingDrawable(Drawable drawable, LayoutParams params) {
    mLeadingDrawable = drawable;
    if (mLeading != null) {
      mLeading.setImageDrawable(mLeadingDrawable);
      mLeading.setLayoutParams(params);
      return;
    }
    mLeading = new ImageView(getContext());
    mLeading.setImageDrawable(mLeadingDrawable);

    addView(mLeading, 0, params);
  }

  public void setText(String text) {
    mText = text;

    if (mTextView != null) {
      mTextView.setText(text);
      mTextView.setTextAppearance(getContext(), mTextAppearance);
      return;
    }

    mTextView = new TextView(getContext());
    mTextView.setText(text);
    mTextView.setEllipsize(TextUtils.TruncateAt.END);
    mTextView.setTextAppearance(getContext(), mTextAppearance);
    LinearLayout.LayoutParams titleLayout = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
    titleLayout.gravity = Gravity.CENTER_VERTICAL;
    addView(mTextView, titleLayout);
  }

  protected int px(int size) {
    return Utils.px(size, getContext());
  }

  @Override
  public void setChecked(boolean checked) {
    this.mChecked = checked;

    try {
      if (checked) {
        mLeading.setImageTintList(new ColorStateList(new int[][]{new int[]{}}, new int[]{mLeadingDrawableActiveColor}));
      }else {
        mLeading.setImageTintList(new ColorStateList(new int[][]{new int[]{}}, new int[]{mLeadingDrawableDefaultColor}));
      }
    } catch (Exception ignored) {
    }

    refreshDrawableState();
  }

  @Override
  protected int[] onCreateDrawableState(int extraSpace) {
    final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);


    if (isChecked()) {
      mergeDrawableStates(drawableState, CHECKED_STATE_SET);
    }

    return drawableState;
  }

  @Override
  public boolean isChecked() {
    return this.mChecked;
  }

  @Override
  public void toggle() {
    this.setChecked(!this.mChecked);
  }
}
