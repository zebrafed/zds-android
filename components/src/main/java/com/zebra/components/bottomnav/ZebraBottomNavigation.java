package com.zebra.components.bottomnav;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.zebra.components.R;

import java.lang.reflect.Field;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;


public class ZebraBottomNavigation extends BottomNavigationView {

    private int height;
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_BottomNavigation;
    private static final String TAG = "com.zebra.components.bottomnav.ZebraBottomNavigation";

    public ZebraBottomNavigation(@NonNull Context context) {
        this(context, null);
    }

    public ZebraBottomNavigation(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraBottomNavigation);
    }

    //todo add some margin between icon and text on mobile only!

    public ZebraBottomNavigation(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {

        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraBottomNavigation, defStyleAttr, DEF_STYLE_RES);

        height = attributes.getDimensionPixelOffset(R.styleable.ZebraBottomNavigation_android_height, px(58, context));

        this.setItemHorizontalTranslationEnabled(false);
        if (getLabelVisibilityMode() == LabelVisibilityMode.LABEL_VISIBILITY_LABELED) {
            this.setItemIconSize(getResources().getDimensionPixelSize(R.dimen.bottom_icon));
            if (Mini(context)) {
                this.setItemTextAppearanceActive(R.style.Widget_Mini_BottomNavigation_text);
                this.setItemTextAppearanceInactive(R.style.Widget_Mini_BottomNavigation_text);
            } else {
                this.setItemTextAppearanceActive(R.style.Widget_Zebra_BottomNavigation_text);
                this.setItemTextAppearanceInactive(R.style.Widget_Zebra_BottomNavigation_text);
            }
        }
        attributes.recycle();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewGroup.LayoutParams lps = getLayoutParams();
        lps.height = height;
        boolean flag = false;
        int size = getMenu().size();

        if (!Mini(getContext()) && getLabelVisibilityMode() == LabelVisibilityMode.LABEL_VISIBILITY_LABELED) {
            for (int i = 0; i < size; i++) {
                if (size > 4) {
                    flag = true;
                    getMenu().getItem(i).setTitle("");
                }
            }

            Context context = getContext();
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) getChildAt(0);

            for (int l = 0; l < menuView.getChildCount(); l++) {
                View menuItem = menuView.getChildAt(l);
                View small = menuItem.findViewById(R.id.smallLabel);
                View large = menuItem.findViewById(R.id.largeLabel);
                small.setPadding(0, px(24, context), 0, 0);
                large.setPadding(0, px(24, context), 0, 0);
            }
            //todo there needs to be a bigger gap between the icons and the titles.
            // This can't be added because the the menuView has a fixed height that cant be changed
            // Adding more padding means the lower parts of the text get cut off.
        }
        if (flag) {
            Log.e(TAG, "The maximum number of icons is 4 with text");
        }

        setLayoutParams(lps);
    }
}
