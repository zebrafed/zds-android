# Standard button example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:text="Button" />
```

# Disabled button example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:enabled="false"
    android:text="Disabled button" />
```

# Big size standard button example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="64dp"
    android:text="Button" />
```

# Icon button example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:drawableEnd="@drawable/ic_sign_in"
    android:text="Button"/>
```

# Icon button big size example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="64dp"
    android:drawableEnd="@drawable/ic_sign_in"
    android:text="Button"/>
```

# Icon button, text left aligned example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:drawableEnd="@drawable/ic_right"
    android:textAlignment="textStart"
    android:text="@string/button" />
```

# Icon button, text left aligned big size example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="64dp"
    android:drawableEnd="@drawable/ic_right"
    android:textAlignment="textStart"
    android:text="@string/button"/>

```

# Pagination buttons without numbers
```xml
<com.zebra.components.button.ZebraPageButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:pageNumber="3"
    app:totalPages="5" />
```

# Pagination buttons with numbers
```xml
<com.zebra.components.button.ZebraPageButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:pageNumbers="true"
    app:pageNumber="3"
    app:totalPages="5" />
```

# Button styles

The following Zebra styles for buttons can be applied:
```xml
//todo
```