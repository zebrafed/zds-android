package com.zebra.components.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.zebra.components.R;

import java.text.MessageFormat;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;

public class ZebraPageButton extends LinearLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Page_Button;
    private static final String TAG = "com.zebra.components.button.ZebraPageButton";
    private ZebraButton prev, next;
    private TextView text;
    private int pageNumber, totalPages, color, textColor, buttonWidth, buttonHeight, padding, backgroundColor;
    private boolean pageNumbers;
    private LayoutParams lps;

    public ZebraPageButton(Context context) {
        this(context, null);
    }

    public ZebraPageButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraPageButton);
    }

    public ZebraPageButton(final Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraPageButton, defStyleAttr, DEF_STYLE_RES);

        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setClickable(true);

        pageNumber = attributes.getInt(R.styleable.ZebraPageButton_pageNumber, 1);
        totalPages = attributes.getInt(R.styleable.ZebraPageButton_totalPages, 1);
        pageNumbers = attributes.getBoolean(R.styleable.ZebraPageButton_pageNumbers, false);

        padding = px(8, context);
        if (Mini(context)) padding = px(4, context);

        buttonWidth = attributes.getDimensionPixelOffset(R.styleable.ZebraPageButton_buttonWidth, px(96, context));
        buttonHeight = attributes.getDimensionPixelOffset(R.styleable.ZebraPageButton_buttonHeight, px(48, context));
        color = attributes.getColor(R.styleable.ZebraPageButton_android_color, context.getColor(R.color.white));
        textColor = attributes.getColor(R.styleable.ZebraButton_android_textColor, context.getColor(R.color.white));
        backgroundColor = attributes.getColor(R.styleable.ZebraPageButton_android_colorBackground, context.getColor(R.color.primaryColor));
        prev = makeButtons(context, true);
        next = makeButtons(context, false);
        lps = new LayoutParams(buttonWidth, buttonHeight);

        this.addView(prev);
        if (pageNumbers) {
            this.setText(context);
            this.addView(text);
        }
        this.addView(next);
        attributes.recycle();
    }

    private void setText(Context context) {
        text = new TextView(context);
        text.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        text.setTextColor(textColor);
        text.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_medium));
        LayoutParams lps3;
        if (Mini(context)) {
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            lps3 = new LayoutParams(px(32, context), buttonHeight);

        } else {
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            lps3 = new LayoutParams(px(64, context), buttonHeight);
        }

        text.setLayoutParams(lps3);
        text.setText(MessageFormat.format("{0}{1}{2}", pageNumber, context.getString(R.string.div), totalPages));
        text.setGravity(Gravity.CENTER);

    }

    private void setButtonStatus(Context context) {
        if (pageNumber > 1) {
            prev.setBackgroundColor(context.getColor(R.color.primaryColor));
            prev.setEnabled(true);
        } else {
            prev.setBackgroundColor(context.getColor(R.color.gray_300));
            prev.setEnabled(false);
        }

        if (pageNumber != totalPages) {
            next.setBackgroundColor(context.getColor(R.color.primaryColor));
            next.setEnabled(true);
        } else {
            next.setBackgroundColor(context.getColor(R.color.gray_300));
            next.setEnabled(false);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        applyAttrs();
    }

    private void applyAttrs() {
        Context context = getContext();
        if (!Mini(context)) {
            if (pageNumbers) {
                buttonWidth = (getWidth() / 2) - px(32, context);
                lps.width = buttonWidth;
                prev.setLayoutParams(lps);
                next.setLayoutParams(lps);
            } else {
                buttonWidth = (getWidth() / 2) - px(6, context);
                lps.width = buttonWidth;

                LayoutParams prevLPs = lps;
                LayoutParams nextLPs = lps;

                prevLPs.setMarginEnd(px(8, context));
                nextLPs.setMarginStart(px(8, context));
                prev.setLayoutParams(prevLPs);
                next.setLayoutParams(nextLPs);
            }
        } else {
            prev.setLayoutParams(lps);
            next.setLayoutParams(lps);
        }

        prev.setPadding(padding, 0, 0, 0);
        next.setPadding(0, 0, padding, 0);
        prev.setCompoundDrawablePadding(padding);
        next.setCompoundDrawablePadding(padding);

        if (totalPages == pageNumber) {
            next.setEnabled(false);
        } else {
            next.setBackgroundColor(backgroundColor);
            next.setTextColor(color);
            next.getDrawableEnd().setTint(color);
        }
        if (pageNumber == 1) {
            prev.setEnabled(false);
        } else {
            prev.setBackgroundColor(backgroundColor);
            prev.setTextColor(color);
            prev.getDrawableStart().setTint(color);
        }
    }

    private ZebraButton makeButtons(Context context, boolean isPrev) {

        ZebraButton button = new ZebraButton(context);
        button.setTextColor(context.getColor(R.color.white));
        button.setBackgroundColor(context.getColor(R.color.primaryColor));
        button.setTextColor(context.getColor(R.color.white));
        Drawable icon;

        if (isPrev) {
            icon = context.getDrawable(R.drawable.ic_left);
            button.setText(context.getString(R.string.previous));
            button.setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
            button.setDrawableStart(icon);
        } else {
            button.setText(context.getString(R.string.next));
            icon = context.getDrawable(R.drawable.ic_right);
            button.setTextAlignment(TEXT_ALIGNMENT_TEXT_END);
            button.setDrawableEnd(icon);
        }

        if (Mini(getContext())) {
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }
        return button;
    }

    public ZebraButton getPreviousButton() {
        return this.prev;
    }

    public ZebraButton getNextButton() {
        return this.next;
    }

    public int getPageNumber() {
        return this.pageNumber;
    }

    public int getTotalPages() {
        return this.totalPages;
    }

    public TextView getTextView() {
        return this.text;
    }

    public void setPreviousButton(ZebraButton button) {
        this.prev = button;
    }

    public void setNextButton(ZebraButton button) {
        this.next = button;
    }

    public void setPageNumber(int number) {
        this.pageNumber = number;
        this.setText(getContext());
        this.setButtonStatus(getContext());
    }

    public void setTotalPages(int number) {
        this.totalPages = number;
        this.setText(getContext());
        this.setButtonStatus(getContext());
    }
}