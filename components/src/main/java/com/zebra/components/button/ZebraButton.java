package com.zebra.components.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;

public class ZebraButton extends AppCompatButton {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Button;
    private static final String TAG = "com.zebra.components.button.ZebraButton";
    private Drawable drawableStart, drawableEnd;
    private int mTextColor, textAlignment, colorBackground, disabledBackgroundColor, disabledTextColor;
    private boolean isMini;

    public ZebraButton(@NonNull Context context) {
        this(context, null);
    }

    public ZebraButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraButton);
    }

    public ZebraButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraButton, defStyleAttr, DEF_STYLE_RES);
        setTransformationMethod(null);
        isMini = Mini(context);

        drawableStart = attributes.getDrawable(R.styleable.ZebraButton_android_drawableStart);
        drawableEnd = attributes.getDrawable(R.styleable.ZebraButton_android_drawableEnd);

        mTextColor = attributes.getColor(R.styleable.ZebraButton_android_textColor, context.getColor(R.color.white));
        textAlignment = attributes.getInteger(R.styleable.ZebraButton_android_textAlignment, TEXT_ALIGNMENT_CENTER);
        colorBackground = attributes.getColor(R.styleable.ZebraButton_android_colorBackground, context.getColor(R.color.primaryColor));
        disabledBackgroundColor = context.getColor(R.color.gray_300);
        disabledTextColor = context.getColor(R.color.white);
        if (isMini) {
            this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else {
            this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        }
        if (textAlignment == 0) textAlignment = TEXT_ALIGNMENT_CENTER;
        setTextColor(attributes.getColor(R.styleable.ZebraButton_android_textColor, context.getColor(R.color.primaryColor)));
        this.setStateListAnimator(null);
        attributes.recycle();
    }

    public void setDrawableStart(Drawable mIcon) {
        drawableStart = mIcon;
        initDrawables();
    }

    public void setDrawableEnd(Drawable mIcon) {
        drawableEnd = mIcon;
        initDrawables();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        initDrawables();
    }

    private void initDrawables() {

        int eight = px(8, getContext());
        int twenty_four = px(24, getContext());
        int thirty_two = px(32, getContext());
        int forty_eight = px(48, getContext());

        int paddingStart = getPaddingStart();
        int paddingEnd = getPaddingEnd();

        //Leading icon
        if (drawableStart != null) {
            drawableStart.mutate();


            if (this.isEnabled()) {
                drawableStart.setTint(mTextColor);
            } else {
                drawableStart.setTint(disabledTextColor);
            }

            if (isMini) {


                drawableStart.setBounds(0, 0, twenty_four, twenty_four);
                if (drawableEnd == null) paddingEnd = thirty_two;
                if (this.isEnabled()) {
                    drawableStart.setTint(mTextColor);
                } else {
                    drawableStart.setTint(disabledTextColor);
                }
            } else {
                drawableStart.setBounds(0, 0, thirty_two, thirty_two);
                if (drawableEnd == null) paddingEnd = forty_eight;

            }

            this.setCompoundDrawablePadding(eight);
            if (!this.isEnabled()) drawableStart.setAlpha(102);
        }

        //Ending icon
        if (drawableEnd != null) {
            if (this.isEnabled()) {
                drawableEnd.setTint(mTextColor);
            } else {
                drawableEnd.setTint(disabledTextColor);
            }
            drawableEnd.mutate();
            if (isMini) {
                drawableEnd.setBounds(0, 0, twenty_four, twenty_four);
                if ((drawableStart == null) && (textAlignment == TEXT_ALIGNMENT_CENTER)) {
                    paddingStart = thirty_two;
                }
            } else {
                drawableEnd.setBounds(0, 0, thirty_two, thirty_two);
                if (drawableStart == null && (textAlignment == TEXT_ALIGNMENT_CENTER)) {
                    paddingStart = forty_eight;

                }
            }
            if (!this.isEnabled()) drawableEnd.setAlpha(102);
        }

        this.setPadding(paddingStart, 0, paddingEnd, 0);
        this.setCompoundDrawables(drawableStart, null, drawableEnd, null);

        if (!this.isEnabled()) {
            setBackgroundColor(disabledBackgroundColor);
            setTextColor(disabledTextColor);
        } else {
            setBackgroundColor(colorBackground);
            setTextColor(mTextColor);
        }
    }

    Drawable getDrawableStart() {
        return drawableStart;
    }

    Drawable getDrawableEnd() {
        return drawableEnd;
    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);
        this.colorBackground = color;
    }

    @Override
    public void setTextColor(int textColor) {
        super.setTextColor(textColor);
        this.mTextColor = textColor;
    }
}