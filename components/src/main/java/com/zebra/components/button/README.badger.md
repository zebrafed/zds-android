# Default Button Examples
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:text="Full Button" 
    android:textColor="@color/colorPrimary" />
```

# Half sized button example
```xml
<LinearLayout
    android:layout_width="match_parent"
    android:layout_marginBottom="4dp"
    android:layout_height="wrap_content"
    android:orientation="horizontal">
    
    <com.zebra.components.button.ZebraButton
        android:layout_width="110dp"
        android:layout_height="wrap_content"
        android:layout_marginRight="4dp"
        android:background="@color/white"
        android:text="Half button"
        android:textColor="@color/textColor" />
    
    <com.zebra.components.button.ZebraButton
        android:layout_width="110dp"
        android:layout_height="wrap_content"
        android:background="@color/white"
        android:text="Half button"
        android:textColor="@color/colorPrimary" />
 </LinearLayout>
```

# Arrow button example
```xml
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="@color/colorPrimary"
    android:drawableEnd="@drawable/ic_right"
    android:text="@string/color_button"
    android:textAlignment="viewStart"
    android:textColor="@color/white" />
```

# Status button example
```xml 
<com.zebra.components.button.ZebraButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    android:background="@color/colorPrimary"
    android:drawableStart="@drawable/ic_good"
    android:drawableEnd="@drawable/right"
    android:text="@string/color_button"
    android:textAlignment="viewStart"
    android:textColor="@color/white"
    custom:isStatusButton="true" />

```

# Pagination button without page numbers example
```xml
 <com.zebra.components.button.ZebraPageButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:pageNumber="1"
    app:totalPages="3" />
```

Button values can be changed programmatically
```java
zebraPageButton.setPageNumber(1);
zebraPageButton.setTotalPages(8);

ZebraButton prev = zebraPageButton.getPreviousButton();
ZebraButton next = zebraPageButton.getNextButton();

prev.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        zebraPageButton.setPageNumber((zebraPageButton.getPageNumber())-1);
    }
});

next.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        zebraPageButton.setPageNumber((zebraPageButton.getPageNumber())+1);
    }
});

```

# Pagination button with page numbers example
```xml
 <com.zebra.components.button.ZebraPageButton
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:pageNumbers="true"
    app:pageNumber="1"
    app:totalPages="3" />
```

Button values can be changed programmatically
```java
zebraPageButton.setPageNumber(1);
zebraPageButton.setTotalPages(8);

ZebraButton prev = zebraPageButton.getPreviousButton();
ZebraButton next = zebraPageButton.getNextButton();

prev.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        zebraPageButton.setPageNumber((zebraPageButton.getPageNumber())-1);
    }
});

next.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        zebraPageButton.setPageNumber((zebraPageButton.getPageNumber())+1);
    }
});

```