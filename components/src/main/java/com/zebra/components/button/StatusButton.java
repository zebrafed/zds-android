package com.zebra.components.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;


//TODO add status options?
public class StatusButton extends ZebraButton {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Button;
    private Drawable statusDrawable;

    public StatusButton(@NonNull Context context) {
        this(context, null);
    }

    public StatusButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraButton);
    }

    public StatusButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        setPadding(0, 0, getPaddingRight(), 0);
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraButton, defStyleAttr, DEF_STYLE_RES);
        statusDrawable = attributes.getDrawable(R.styleable.ZebraButton_android_drawableStart);
        setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
        attributes.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        statusDrawable.setBounds(0, 0, px(48, getContext()), px(48, getContext()));
        setCompoundDrawables(statusDrawable, null, getDrawableEnd(), null);
    }

    public void setStatusDrawable(Drawable d) {
        d.setBounds(0, 0, px(48, getContext()), px(48, getContext()));
        setCompoundDrawables(statusDrawable, null, getDrawableEnd(), null);
    }
}
