package com.zebra.components.textview;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class ZebraTextView extends AppCompatTextView {
    public ZebraTextView(Context context) {
        super(context);
    }

    public ZebraTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

//    todo is this needed?
    public ZebraTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
