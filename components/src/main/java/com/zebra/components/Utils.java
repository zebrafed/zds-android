package com.zebra.components;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import androidx.annotation.AttrRes;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.StyleableRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ContextThemeWrapper;

public class Utils {
    private static final int[] ANDROID_THEME_OVERLAY_ATTRS = new int[]{android.R.attr.theme, R.attr.theme};
    private static final int[] THEME_CARD_ATTR = new int[]{R.attr.ZebraCardView};
    private static final String TAG = "Utils";

    public static Context createThemedContext(Context context, AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        Log.e("TAG", "start");

        int ZebraThemeCardId =
                obtainThemeCardId(context, attrs, defStyleAttr, defStyleRes);
        //TODO: Why doesn't this work?
        if (ZebraThemeCardId != 0
                && (!(context instanceof ContextThemeWrapper)
                || ((ContextThemeWrapper) context).getThemeResId() != ZebraThemeCardId)) {
            // If the context isn't a ContextThemeWrapper, or it is but does not have the same theme as we
            // need, wrap it in a new wrapper.
            context = new ContextThemeWrapper(context, ZebraThemeCardId);

            // We want values set in android:theme or app:theme to always override values supplied by
            // materialThemeOverlay, so we'll wrap the context again if either of those are set.
            int androidThemeOverlayId = obtainAndroidThemeOverlayId(context, attrs);
            if (androidThemeOverlayId != 0) {
                context = new ContextThemeWrapper(context, androidThemeOverlayId);
            }
        }


        return context;
    }

    @StyleRes
    private static int obtainAndroidThemeOverlayId(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, ANDROID_THEME_OVERLAY_ATTRS);
        int androidThemeId = a.getResourceId(0, 0);
        int appThemeId = a.getResourceId(1, 0);
        a.recycle();
        if (androidThemeId != 0) {
            return androidThemeId;
        } else {
            return appThemeId;
        }
    }

    @StyleRes
    private static int obtainThemeCardId(Context context, AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        TypedArray a =
                context.obtainStyledAttributes(
                        attrs, THEME_CARD_ATTR, defStyleAttr, defStyleRes);
        int ZebraThemeCardId = a.getResourceId(0, 0);
        a.recycle();
        return ZebraThemeCardId;
    }

    @Nullable
    public static Drawable getDrawable(Context context, TypedArray attributes, @StyleableRes int index) {
        if (attributes.hasValue(index)) {
            int resourceId = attributes.getResourceId(index, 0);
            if (resourceId != 0) {
                Drawable value = AppCompatResources.getDrawable(context, resourceId);
                if (value != null) {
                    return value;
                }
            }
        }
        return attributes.getDrawable(index);
    }

    public static TypedArray obtainStyledAttributes(
            Context context,
            AttributeSet set,
            @StyleableRes int[] attrs,
            @AttrRes int defStyleAttr,
            @StyleRes int defStyleRes,
            @StyleableRes int... textAppearanceResIndices) {

        checkTheme(context);

        // Then, safely retrieve the styled attribute information.
        return context.getTheme().obtainStyledAttributes(set, attrs, defStyleAttr, defStyleRes);
    }

    @Nullable
    public static ColorStateList getColorStateList(
            Context context, TypedArray attributes, @StyleableRes int index) {
        if (attributes.hasValue(index)) {
            int resourceId = attributes.getResourceId(index, 0);
            if (resourceId != 0) {
                ColorStateList value = AppCompatResources.getColorStateList(context, resourceId);
                if (value != null) {
                    return value;
                }
            }
        }

        // Reading a single color with getColorStateList() on API 15 and below doesn't always correctly
        // read the value. Instead we'll first try to read the color directly here.
        if (VERSION.SDK_INT <= VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            int color = attributes.getColor(index, -1);
            if (color != -1) {
                return ColorStateList.valueOf(color);
            }
        }

        return attributes.getColorStateList(index);
    }

    public static void checkTheme(Context context) {
        TypedValue isZebraTheme = new TypedValue();
        boolean resolvedValue = context.getTheme().resolveAttribute(R.attr.Mini, isZebraTheme, true);
        if (!resolvedValue) {
            throw new IllegalArgumentException(
                    "The style on this component requires your app theme to be "
                            + "Theme.Zebra"
                            + " (or a descendant).");
        }
    }

    private static boolean isTheme(Context context, int[] themeAttributes) {
        TypedArray a = context.obtainStyledAttributes(themeAttributes);
        for (int i = 0; i < themeAttributes.length; i++) {
            if (!a.hasValue(i)) {
                a.recycle();
                return false;
            }
        }
        a.recycle();
        return true;
    }

    public static int px(int value, Context context) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                value,
                context.getResources().getDisplayMetrics()
        );
    }

    public static boolean Mini(Context context) {
        TypedValue tv = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.Mini, tv, false);
        return tv.data != 0;
    }
}
