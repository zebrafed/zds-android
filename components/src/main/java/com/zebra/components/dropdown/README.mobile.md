# Basic Mobile Dropdown Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:menu="@menu/dropdown_demo"
    >

    <TextView
        style="@style/Widget_Zebra_Dropdown.TitleAppearance"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:paddingStart="8dp"
        android:paddingTop="14dp"
        android:paddingBottom="14dp"
        android:text="Dropdown"
        android:textStyle="bold" />
</com.zebra.Zebra.dropdown.ZebraDropdown>
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />

</menu>
```

# Mobile Dropdown Example with already selected value

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:label="Type:"
    app:value="Dropdown"
    app:menu="@menu/dropdown_demo"
    >
</com.zebra.Zebra.dropdown.ZebraDropdown>
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />

</menu>
```