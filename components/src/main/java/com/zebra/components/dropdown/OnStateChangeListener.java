package com.zebra.components.dropdown;

public interface OnStateChangeListener {
  void onOpen();
  void onClose();
}
