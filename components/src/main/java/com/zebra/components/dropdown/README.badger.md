# Basic Dropdown Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:menu="@menu/dropdown_demo"
    />
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />
</menu>
```

# Dropdown Example with already selected value

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:value="Details"
    app:menu="@menu/dropdown_demo"
    />
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />
</menu>
```

# Dropdown Example with leading icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:value="Details"
    app:menu="@menu/dropdown_demo"
    />
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />
</menu>
```

# Dropdown Example with many options

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.dropdown.ZebraDropdown
    android:id="@+id/custom_dropdown"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:menu="@menu/dropdown_demo"
    >

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:id="@+id/custom_dropdown_header"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@android:color/transparent"
        app:icon="@drawable/icon_placeholder"
        app:subheader="Details"
        app:title="Type" />
</com.zebra.Zebra.dropdown.ZebraDropdown>
```
```xml
<!-- @menu/dropdown_demo -->
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_1"
        android:title="Show all notification lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_2"
        android:title="Hide sensitive lorem ipsum dolor sit amet" />
    <item
        android:id="@+id/nav_3"
        android:title="Don’t show notifications at all" />
</menu>
```
```java
ZebraDropdown dropdown = view.findViewById(R.id.custom_dropdown);
final ZebraCardHeaderView header = view.findViewById(R.id.custom_dropdown_header);

dropdown.setOnItemClickListener(new OnItemClickListener() {
    @Override
    public void onItemClick(View view, int position, long id, MenuItem item) {
        header.setSubHeader(item.getText());
    }
});
```