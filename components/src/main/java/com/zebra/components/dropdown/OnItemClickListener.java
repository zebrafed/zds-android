package com.zebra.components.dropdown;

import android.view.View;

import com.zebra.components.menu.MenuItem;

public interface OnItemClickListener {
  void onItemClick(View view, int position, long id, MenuItem item);
}
