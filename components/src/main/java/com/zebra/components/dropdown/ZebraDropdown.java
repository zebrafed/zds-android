package com.zebra.components.dropdown;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;

import com.zebra.components.R;
import com.zebra.components.Utils;
import com.zebra.components.card.ZebraCardHeaderView;
import com.zebra.components.menu.ZebraMenuPopup;
import com.zebra.components.menu.MenuItem;
import com.zebra.components.ripple.ZebraRipple;

import java.util.ArrayList;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;


public class ZebraDropdown extends FrameLayout implements View.OnClickListener, OnItemClickListener {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_Dropdown;
  private final ZebraRipple mArrow;
  private int mArrowTint = -1;
  private String mLabel;
  private String mValue;
  private final Drawable mActiveBackground;
  private final Drawable mDefaultBackground;
  private final Drawable mArrowDown;
  private final Drawable mArrowUp;
  private boolean isSelected = false;
  private LinearLayout contentLayout;
  private boolean mIsDefaultLayout = true;
  private ZebraCardHeaderView mContentView;
  private OnStateChangeListener mOnStateChangeListener;
  private OnItemClickListener mOnItemClickListener;
  private boolean isOpen = false;
  private int mMenuRes;

  public ZebraDropdown(@NonNull Context context) {
    this(context, null /* attrs */);
  }

  public ZebraDropdown(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraDropdown);
  }

  public ZebraDropdown(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraDropdown, defStyleAttr, DEF_STYLE_RES);

    int padding = attributes.getDimensionPixelSize(R.styleable.ZebraDropdown_android_padding, 0);


    mArrowDown = attributes.getDrawable(R.styleable.ZebraDropdown_arrow_down);
    mArrowUp = attributes.getDrawable(R.styleable.ZebraDropdown_arrow_up);

    mActiveBackground = attributes.getDrawable(R.styleable.ZebraDropdown_activeBackground);
    mDefaultBackground = attributes.getDrawable(R.styleable.ZebraDropdown_defaultBackground);

    if (attributes.hasValue(R.styleable.ZebraDropdown_arrow_tint)) {
      mArrowTint = attributes.getColor(R.styleable.ZebraDropdown_arrow_tint, getResources().getColor(R.color.primaryColor));
    }

    mLabel = attributes.getString(R.styleable.ZebraDropdown_label);
    mValue = attributes.getString(R.styleable.ZebraDropdown_value);
    mMenuRes = attributes.getResourceId(R.styleable.ZebraDropdown_menu, -1);

    boolean isSelected = attributes.getBoolean(R.styleable.ZebraDropdown_selected, mValue != null);


    setPadding(padding, padding, padding, padding);

    contentLayout = new LinearLayout(context);
    contentLayout.setGravity(Gravity.CENTER_VERTICAL);
    contentLayout.setOrientation(LinearLayout.HORIZONTAL);
    contentLayout.setLayoutDirection(LinearLayout.LAYOUT_DIRECTION_RTL);
    FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
    contentLayout.setBackground(mDefaultBackground);
    contentLayout.setPadding(0, 0, 0, px(2));

    mArrow = new ZebraRipple(context);
    mArrow.setImageDrawable(mArrowDown);
    mArrow.setDuplicateParentStateEnabled(true);
    mArrow.setClickable(false);

    contentLayout.addView(mArrow);

    mContentView = new ZebraCardHeaderView(context);
    mContentView.setBackground(null);


    if (mLabel != null && !mLabel.isEmpty()) {
      setLabel(mLabel);
    }

    if (mValue != null && !mValue.isEmpty()) {
      setValue(mValue);
    }

    ViewGroup.LayoutParams contentLp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1.0f);
    contentLayout.addView(mContentView, contentLp);

    setSelected(isSelected);

    super.addView(contentLayout, -1, lp);

    contentLayout.setOnClickListener(this);

    attributes.recycle();
  }

  @Override
  public void setOnClickListener(@Nullable OnClickListener l) {
    contentLayout.setOnClickListener(l);
  }

  public void setOnStateChangeListener(@Nullable OnStateChangeListener l) {
    mOnStateChangeListener = l;
  }

  public void setOnItemClickListener(@Nullable OnItemClickListener l) {
    mOnItemClickListener = l;
  }

  public void setLabel(String text) {
    mLabel = text;
    if (mContentView != null) {

      TypedArray attributes = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.dropdownTitle, R.attr.dropdownTitleActive});
      int titleAppearance = attributes.getResourceId(0, R.style.Widget_Zebra_Dropdown_TitleAppearance);
      int titleAppearanceActive = attributes.getResourceId(1, R.style.Widget_Mini_Dropdown_TitleAppearance_Active);

      mContentView.setTitle(text, isSelected ? titleAppearanceActive: titleAppearance);
    }
  }

  public void setMenuResource(int res) {
    mMenuRes = res;
  }

  public void setValue(String text) {
    mValue = text;
    if (mContentView != null) {
      TypedArray attributes = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.dropdownSubheader});
      int subHeaderAppearance = attributes.getResourceId(0, R.style.Widget_Zebra_Dropdown_SubheaderAppearance);

      mContentView.setSubHeader(text, subHeaderAppearance);
    }
    setSelected(true);
  }

  public void setOpen(boolean open) {
    isOpen = open;

    if (isOpen) {
      mArrow.setImageDrawable(mArrowUp);
    } else {
      mArrow.setImageDrawable(mArrowDown);
    }
  }

  @SuppressLint("ResourceType")
  public void setSelected(boolean selected) {
    isSelected = selected;
    setLabel(mLabel);

    TypedArray colors = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.colorPrimary, R.attr.textColor});

    mArrow.setActive(isSelected);

    if (isSelected) {
      mArrow.setColor(colors.getColor(0, R.color.primaryColor));
      contentLayout.setBackground(mActiveBackground);
    } else {
      mArrow.setColor(colors.getColor(1, R.color.gray_900));
      contentLayout.setBackground(mDefaultBackground);
    }

  }

  private int px(int value) {
    return Utils.px(value, getContext());
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    if (mIsDefaultLayout) {
      contentLayout.removeView(mContentView);
      mContentView = null;
    }
    mIsDefaultLayout = false;
    ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1.0f);
    contentLayout.addView(child, index, lp);
  }

  @Override
  public void addView(View child) {
    if (mIsDefaultLayout) {
      contentLayout.removeView(mContentView);
      mContentView = null;
    }
    mIsDefaultLayout = false;
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1.0f);
    child.setLayoutParams(lp);
    contentLayout.addView(child);
  }

  @Override
  public void addView(View child, ViewGroup.LayoutParams params) {
    if (mIsDefaultLayout) {
      contentLayout.removeView(mContentView);
      mContentView = null;
    }
    mIsDefaultLayout = false;
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1.0f);
    contentLayout.addView(child, lp);
  }

  @Override
  public void onClick(View v) {
    setOpen(!isOpen);

    if (isOpen) {
      ZebraMenuPopup popup = new ZebraMenuPopup(getContext());

      MenuInflater menuInflater = new MenuInflater(getContext());

      PopupMenu p  = new PopupMenu(getContext(), null);
      Menu menu = p.getMenu();
      menuInflater.inflate(mMenuRes, menu);

      ArrayList<MenuItem> items = new ArrayList<>();

      for (int i = 0; i < menu.size(); i++) {
        android.view.MenuItem item = menu.getItem(i);
        items.add(new MenuItem(i, item.getTitle().toString()));
      }

      popup.setItems(items);
      popup.setOnItemClickListener(this);

      popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
        @Override
        public void onDismiss() {
          setOpen(false);
        }
      });
      popup.show(v);
    }

    if (mOnStateChangeListener != null) {
      if (isOpen) {
        mOnStateChangeListener.onOpen();
      } else {
        mOnStateChangeListener.onClose();
      }
    }
  }

  @Override
  public void onItemClick(View view, int position, long id, MenuItem item) {
    setValue(item.getText());
    if (mOnItemClickListener != null) {
      mOnItemClickListener.onItemClick(view, position, id, item);
    }
  }
}
