package com.zebra.components.drawer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.internal.NavigationMenuPresenter;
import com.google.android.material.internal.NavigationMenuView;
import com.zebra.components.R;

import java.util.ArrayList;

@SuppressLint("RestrictedApi")
public class ZebraNavigationMenuPresenter extends NavigationMenuPresenter {
  private NavigationMenuView menuView;
  private ZebraNavigationMenuAdapter adapter;
  private LayoutInflater layoutInflater;
  private LinearLayout headerLayout;
  private MenuBuilder menu;
  private boolean mPortable;
  final View.OnClickListener onClickListener = new View.OnClickListener() {
    public void onClick(View v) {
      ZebraDrawerMenuItem itemView = (ZebraDrawerMenuItem)v;
      ZebraNavigationMenuPresenter.this.setUpdateSuspended(true);
      MenuItem item = itemView.getItem();
      boolean result = ZebraNavigationMenuPresenter.this.menu.performItemAction(item, ZebraNavigationMenuPresenter.this, 0);
      if (item != null && item.isCheckable() && result) {
        ZebraNavigationMenuPresenter.this.adapter.setCheckedItem(item);
      }

      ZebraNavigationMenuPresenter.this.setUpdateSuspended(false);
      ZebraNavigationMenuPresenter.this.updateMenuView(false);
    }
  };

  @SuppressLint("RestrictedApi")
  public MenuView getMenuView(ViewGroup root) {
    if (this.menuView == null) {
      this.menuView = (NavigationMenuView) this.layoutInflater.inflate(R.layout.drawer_menu, root, false);
      if (this.adapter == null) {
        this.adapter = new ZebraNavigationMenuAdapter();
      }

      this.headerLayout = (LinearLayout) this.layoutInflater.inflate(R.layout.drawer_menu_header, this.menuView, false);
      this.menuView.setAdapter(this.adapter);
    }

    return this.menuView;
  }

  public void initForMenu(Context context, MenuBuilder menu) {
    this.layoutInflater = LayoutInflater.from(context);
    this.menu = menu;
  }

  public void updateMenuView(boolean cleared) {
    if (this.adapter != null) {
      this.adapter.update();
    }
  }

  public View inflateHeaderView(@LayoutRes int res) {
    View view = this.layoutInflater.inflate(res, this.headerLayout, false);
    this.addHeaderView(view);
    return view;
  }

  public void addHeaderView(@NonNull View view) {
    this.headerLayout.addView(view);
    this.menuView.setPadding(0, 0, 0, this.menuView.getPaddingBottom());
  }

  public void removeHeaderView(@NonNull View view) {
    this.headerLayout.removeView(view);
    if (this.headerLayout.getChildCount() == 0) {
      this.menuView.setPadding(0, 0, 0, this.menuView.getPaddingBottom());
    }
  }

  public void setPortable(boolean portable) {
    mPortable = portable;
    this.adapter.update();
  }

  public void setCheckedItem(@NonNull MenuItemImpl item) {
    this.adapter.setCheckedItem(item);
  }

  class ZebraNavigationMenuHeader implements ZebraNavigationMenuItem {}

  class ZebraNavigationMenuItemDefault implements ZebraNavigationMenuItem {
    private MenuItem mItem;

    public ZebraNavigationMenuItemDefault(MenuItem item) {
      mItem = item;
    }

    public MenuItem getItem() {
      return mItem;
    }
  }

  class ZebraNavigationSeparator extends ZebraNavigationMenuItemDefault {
    public ZebraNavigationSeparator(MenuItem item) {
      super(item);
    }
  }

  interface ZebraNavigationMenuItem {}

  private static class HeaderViewHolder extends ZebraNavigationMenuPresenter.ViewHolder {
    public HeaderViewHolder(View itemView) {
      super(itemView);
    }
  }

  private abstract static class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    public ViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class NormalViewHolder extends ZebraNavigationMenuPresenter.ViewHolder {
    public NormalViewHolder(LayoutInflater inflater, ViewGroup parent, View.OnClickListener listener) {
      super(inflater.inflate(R.layout.drawer_menu_item, parent, false));
      this.itemView.setOnClickListener(listener);
    }
  }

  private static class SeparatorViewHolder extends ZebraNavigationMenuPresenter.ViewHolder {
    public SeparatorViewHolder(LayoutInflater inflater, ViewGroup parent) {
      super(inflater.inflate(R.layout.drawer_menu_separator, parent, false));
    }
  }

  class ZebraNavigationMenuAdapter extends RecyclerView.Adapter<ZebraNavigationMenuPresenter.ViewHolder> {

    private ArrayList<ZebraNavigationMenuItem> items = new ArrayList<>();
    private MenuItem checkedItem;

    ZebraNavigationMenuAdapter() {
      this.prepareMenuItems();
    }

    @Override
    public int getItemCount() {
      return items.size();
    }

    protected ZebraNavigationMenuItem getItem(int position) {
      return items.get(position);
    }

    public long getItemId(int position) {
      return (long)position;
    }

    public void update() {
      this.prepareMenuItems();
      this.notifyDataSetChanged();
    }

    private void prepareMenuItems() {
      int i = 0;

      items.clear();
      items.add(new ZebraNavigationMenuHeader());

      for (int totalSize = menu.getVisibleItems().size(); i < totalSize; ++i) {
        MenuItem item = menu.getVisibleItems().get(i);


        if (!item.hasSubMenu()) {
          items.add(new ZebraNavigationMenuItemDefault(item));
        }else {
          SubMenu subMenu = item.getSubMenu();

          if (i != 0) {
            items.add(new ZebraNavigationSeparator(item));
          }

          int j = 0;

          for(int size = subMenu.size(); j < size; ++j) {
            MenuItem subMenuItem = subMenu.getItem(j);
            if (subMenuItem.isVisible()) {
              items.add(new ZebraNavigationMenuItemDefault(subMenuItem));
            }
          }
        }
      }
    }

    public int getItemViewType(int position) {
      ZebraNavigationMenuItem item = items.get(position);
      if (item instanceof ZebraNavigationMenuHeader) {
        return 1;
      } else if (item instanceof ZebraNavigationMenuItemDefault) {
        return ((ZebraNavigationMenuItemDefault) item).getItem().hasSubMenu() ? 2 : 0;
      } else {
        throw new RuntimeException("Unknown item type.");
      }
    }


    @NonNull
    @Override
    public ZebraNavigationMenuPresenter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      switch(viewType) {
        case 0:
          return new ZebraNavigationMenuPresenter.NormalViewHolder(ZebraNavigationMenuPresenter.this.layoutInflater, parent, ZebraNavigationMenuPresenter.this.onClickListener);
        case 1:
          return new ZebraNavigationMenuPresenter.HeaderViewHolder(ZebraNavigationMenuPresenter.this.headerLayout);
        case 2:
          return new ZebraNavigationMenuPresenter.SeparatorViewHolder(ZebraNavigationMenuPresenter.this.layoutInflater, parent);
        default:
          return null;
      }
    }

    public void onBindViewHolder(@NonNull ZebraNavigationMenuPresenter.ViewHolder holder, int position) {
      ZebraNavigationMenuItem item = getItem(position);
      switch(this.getItemViewType(position)) {
        case 0:
          ZebraDrawerMenuItem itemView = (ZebraDrawerMenuItem)holder.itemView;
          ZebraNavigationMenuItemDefault itemx = (ZebraNavigationMenuItemDefault)item;
          itemView.setItem(itemx.getItem(), mPortable);
          break;
        case 2:
          TextView title = holder.itemView.findViewById(R.id.section_title);
          ZebraNavigationSeparator itemxs = (ZebraNavigationSeparator)item;
          if (mPortable) {
            title.setText("");
            title.setHeight(0);
          }else {
            title.setText(itemxs.getItem().getTitle());
          }
          break;
        case 1:
      }
    }

    public void setCheckedItem(MenuItem checkedItem) {
      if (this.checkedItem != checkedItem && checkedItem.isCheckable()) {
        if (this.checkedItem != null) {
          this.checkedItem.setChecked(false);
        }

        this.checkedItem = checkedItem;
        checkedItem.setChecked(true);
      }
    }
  }

}
