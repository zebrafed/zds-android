# Mobile Drawer with large header and menu divider with subheader

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.drawer.ZebraNavigationView
    android:id="@+id/nav_view"
    android:layout_width="296dp"
    android:layout_height="match_parent"
    android:layout_gravity="start"
    android:background="@color/whiteColor"
    android:fitsSystemWindows="true"
    app:headerLayout="@layout/nav_header_larger_drawer_demo"
    app:menu="@menu/activity_mobiledrawer_demo_drawer">

</com.zebra.Zebra.drawer.ZebraNavigationView>
```
```xml
<!-- @layout/nav_header_larger_drawer_demo -->

<com.zebra.Zebra.drawer.ZebraDrawerHeader xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="bottom"
    android:orientation="vertical"
    android:paddingBottom="0dp">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.zebra.Zebra.drawer.ZebraAvatar
            android:id="@+id/imageView4"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="16dp"
            app:size="large"
            android:contentDescription="@string/nav_header_desc"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:srcCompat="@mipmap/ic_launcher_round" />

    </androidx.constraintlayout.widget.ConstraintLayout>

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginLeft="8dp"
        android:background="@null"
        app:subheader="Details"
        app:subheaderTextAppearance="@style/Widget_ZebraMobile_Drawer.SubheaderAppearance"
        app:title="Name Surname"
        app:titleTextAppearance="@style/Widget_Zebra_Drawer.TitleAppearance" />

</com.zebra.Zebra.drawer.ZebraDrawerHeader>

```
```xml
<!-- @menu/activity_mobiledrawer_demo_drawer -->
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    >

    <group android:id="@+id/gp1" android:checkableBehavior="single" >
        <item
            android:id="@+id/nav_account"
            android:icon="@drawable/person"
            android:checked="true"
            android:title="@string/account" />
        <item
            android:id="@+id/nav_privacy"
            android:icon="@drawable/hand"
            android:title="@string/privacy" />
        <item
            android:id="@+id/nav_favorites"
            android:icon="@drawable/star"
            android:title="@string/favorites" />
        <item
            android:id="@+id/nav_documents"
            android:icon="@drawable/documents"
            android:title="@string/documents" />
        <item
            android:id="@+id/nav_cloud"
            android:icon="@drawable/cloudy"
            android:title="@string/cloud" />
    </group>

    <item
        android:enabled="true"
        android:title="@string/subheader"
        android:visible="true">
        <menu>
            <item
            android:id="@+id/menuItem5"
            android:icon="@drawable/download"
            android:title="@string/downloads" />
            <item
                android:id="@+id/menuItem6"
                android:icon="@drawable/settings"
                android:title="@string/settings" />

        </menu>
    </item>
</menu>
```

# Mobile Portable Drawer with menu divider

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.drawer.ZebraNavigationView
    android:id="@+id/nav_view"
    android:layout_width="wrap_content"
    android:layout_height="match_parent"
    android:layout_gravity="start"
    android:background="@color/whiteColor"
    android:fitsSystemWindows="true"
    app:portable="true"
    app:headerLayout="@layout/nav_header_small_drawer_demo"
    app:menu="@menu/activity_mobiledrawer_demo_drawer">

</com.zebra.Zebra.drawer.ZebraNavigationView>
```
```xml
<!-- @layout/nav_header_small_drawer_demo -->
<com.zebra.Zebra.drawer.ZebraDrawerHeader xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="bottom"
    android:orientation="vertical" >

        <com.zebra.Zebra.drawer.ZebraAvatar
            android:id="@+id/imageView4"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:contentDescription="@string/nav_header_desc"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:srcCompat="@mipmap/ic_launcher_round" />

</com.zebra.Zebra.drawer.ZebraDrawerHeader>
```
```xml
<!-- @menu/activity_mobiledrawer_demo_drawer -->
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    >

    <group android:id="@+id/gp1" android:checkableBehavior="single" >
        <item
            android:id="@+id/nav_account"
            android:icon="@drawable/person"
            android:checked="true"
            android:title="@string/account" />
        <item
            android:id="@+id/nav_privacy"
            android:icon="@drawable/hand"
            android:title="@string/privacy" />
        <item
            android:id="@+id/nav_favorites"
            android:icon="@drawable/star"
            android:title="@string/favorites" />
        <item
            android:id="@+id/nav_documents"
            android:icon="@drawable/documents"
            android:title="@string/documents" />
        <item
            android:id="@+id/nav_cloud"
            android:icon="@drawable/cloudy"
            android:title="@string/cloud" />
    </group>

    <item
        android:enabled="true"
        android:title="@string/subheader"
        android:visible="true">
        <menu>
            <item
            android:id="@+id/menuItem5"
            android:icon="@drawable/download"
            android:title="@string/downloads" />
            <item
                android:id="@+id/menuItem6"
                android:icon="@drawable/settings"
                android:title="@string/settings" />

        </menu>
    </item>
</menu>
```

# Mobile Drawer Example with small header and divider

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.drawer.ZebraNavigationView
    android:id="@+id/nav_view"
    android:layout_width="296dp"
    android:layout_height="match_parent"
    android:layout_gravity="start"
    android:background="@color/whiteColor"
    android:fitsSystemWindows="true"
    app:headerLayout="@layout/nav_mobile_header_drawer_demo"
    app:menu="@menu/activity_mobiledrawer_demo_drawer">

</com.zebra.Zebra.drawer.ZebraNavigationView>
```
```xml
<!-- @layout/nav_mobile_header_drawer_demo -->
<com.zebra.Zebra.drawer.ZebraDrawerHeader xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="center_vertical"
    android:orientation="horizontal"
    android:paddingTop="25dp"
    android:paddingBottom="0dp">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="wrap_content"
        android:layout_height="wrap_content">

        <com.zebra.Zebra.drawer.ZebraAvatar
            android:id="@+id/imageView4"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="16dp"
            android:contentDescription="@string/nav_header_desc"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:size="medium"
            app:srcCompat="@mipmap/ic_launcher_round" />

    </androidx.constraintlayout.widget.ConstraintLayout>

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="59dp"
        android:background="@null"
        app:subheader="Details"
        app:subheaderTextAppearance="@style/Widget_ZebraMobile_Drawer.SubheaderAppearance"
        app:title="Name Surname"
        app:titleTextAppearance="@style/Widget_Zebra_Drawer.TitleAppearance" />

</com.zebra.Zebra.drawer.ZebraDrawerHeader>
```
```xml
<!-- @menu/activity_mobiledrawer_demo_drawer -->
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    >

    <group android:id="@+id/gp1" android:checkableBehavior="single" >
        <item
            android:id="@+id/nav_account"
            android:icon="@drawable/person"
            android:checked="true"
            android:title="@string/account" />
        <item
            android:id="@+id/nav_privacy"
            android:icon="@drawable/hand"
            android:title="@string/privacy" />
        <item
            android:id="@+id/nav_favorites"
            android:icon="@drawable/star"
            android:title="@string/favorites" />
        <item
            android:id="@+id/nav_documents"
            android:icon="@drawable/documents"
            android:title="@string/documents" />
        <item
            android:id="@+id/nav_cloud"
            android:icon="@drawable/cloudy"
            android:title="@string/cloud" />
    </group>

    <item
        android:enabled="true"
        android:title="@string/subheader"
        android:visible="true">
        <menu>
            <item
            android:id="@+id/menuItem5"
            android:icon="@drawable/download"
            android:title="@string/downloads" />
            <item
                android:id="@+id/menuItem6"
                android:icon="@drawable/settings"
                android:title="@string/settings" />

        </menu>
    </item>
</menu>
```