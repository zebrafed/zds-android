package com.zebra.components.drawer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MenuItem;

import androidx.annotation.Nullable;

import com.zebra.components.R;
import com.zebra.components.dialog.ZebraDialogItem;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraDrawerMenuItem extends ZebraDialogItem {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_DrawerMenuItem;
  private final int mPortableOffset;
  private MenuItem mItem;

  public ZebraDrawerMenuItem(Context context) {
    this(context, null);
  }

  public ZebraDrawerMenuItem(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraDrawerMenuItem);
  }

  public ZebraDrawerMenuItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraDialogItem, defStyleAttr, DEF_STYLE_RES);

    mPortableOffset = attributes.getDimensionPixelSize(R.styleable.ZebraDialogItem_portableOffset, px(12));
  }

  @Override
  public void setLeadingDrawable(Drawable drawable) {
    LayoutParams lp = new LayoutParams(px(24), px(24));
    lp.setMargins(0,0,px(16),0);

    setLeadingDrawable(drawable, lp);
  }

  @Override
  public void setLeadingDrawable(Drawable drawable, LayoutParams lp) {
    if (drawable != null) {
      TypedArray attrs = getContext().getTheme().obtainStyledAttributes(new int[]{
          R.attr.textColorSecondary
      });
      int color = attrs.getColor(0, -1);
      drawable.setTint(color);
      attrs.recycle();
    }

    super.setLeadingDrawable(drawable, lp);
  }

  public void setItem(MenuItem item, boolean portable) {
    mItem = item;
    if (portable) {
      LayoutParams lp = new LayoutParams(px(24), px(24));
      lp.setMargins(mPortableOffset,0,px(16),0);

      setLeadingDrawable(item.getIcon(), lp);
    }else {
      setLeadingDrawable(item.getIcon());
      setText(item.getTitle().toString());
    }
    setChecked(item.isChecked());
  }

  public void setItem(MenuItem item) {
    setItem(item, false);
  }

  public MenuItem getItem() {
    return mItem;
  }
}
