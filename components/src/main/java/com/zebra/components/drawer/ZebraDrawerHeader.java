package com.zebra.components.drawer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;

public class ZebraDrawerHeader extends LinearLayout {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_DrawerHeader;

  public ZebraDrawerHeader(Context context) {
    this(context, null);
  }

  public ZebraDrawerHeader(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraDrawerHeader);
  }

  public ZebraDrawerHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
  }
}
