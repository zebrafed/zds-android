package com.zebra.components.drawer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MenuInflater;
import android.view.SubMenu;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.SubMenuBuilder;

import com.google.android.material.navigation.NavigationView;
import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraNavigationView extends NavigationView {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_NavigationView;
  private final int mPortableSize;
  private boolean mPortable;
  private ZebraNavigationMenuPresenter presenter;
  private NavigationMenu menu;
  private MenuInflater menuInflater;
  private int mResId;
  private int mHeaderRes;

  public ZebraNavigationView(Context context) {
    this(context, null);
  }

  public ZebraNavigationView(Context context, AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraNavigationView);
  }

  public ZebraNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraDrawer, defStyleAttr, DEF_STYLE_RES);

    final boolean portable = attributes.getBoolean(R.styleable.ZebraDrawer_portable, false);
    mPortableSize = attributes.getDimensionPixelSize(R.styleable.ZebraDrawer_portable_size, 0);

    this.menu = new NavigationMenu(context);
    this.presenter = new ZebraNavigationMenuPresenter();
    this.presenter.initForMenu(context, this.menu);

    this.menu.addMenuPresenter(this.presenter);
    this.addView((View)this.presenter.getMenuView(this));

    inflateMenu(mResId);
    inflateHeaderView(mHeaderRes);

    setPortable(portable);

    attributes.recycle();
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if (mPortable) {
      super.onMeasure(MeasureSpec.makeMeasureSpec(mPortableSize, MeasureSpec.EXACTLY), heightMeasureSpec);
    }else {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
  }

  void setPortable(boolean portable) {
    mPortable = portable;
    if (this.presenter != null)  {
      this.presenter.setPortable(portable);
    }
    requestLayout();
  }

  @Override
  public void inflateMenu(int resId) {
    mResId = resId;
    if (this.menu == null) {
      return;
    }
    if (this.presenter == null) {
      return;
    }
    this.getMenuInflater().inflate(resId, this.menu);
    this.presenter.updateMenuView(false);
  }

  @Override
  public View inflateHeaderView(@LayoutRes int res) {
    mHeaderRes = res;
    if (this.presenter == null) {
      return null;
    }
    return this.presenter.inflateHeaderView(res);
  }

  @SuppressLint("RestrictedApi")
  private MenuInflater getMenuInflater() {
    if (menuInflater == null) {
      menuInflater = new SupportMenuInflater(getContext());
    }

    return menuInflater;
  }

  @SuppressLint("RestrictedApi")
  public class NavigationMenu extends MenuBuilder {
    private final Context mContext;

    public NavigationMenu(Context context) {
      super(context);
      mContext = context;
    }

    @Override
    public void addMenuPresenter(MenuPresenter presenter) {
      super.addMenuPresenter(presenter);
    }

    @Override
    public SubMenu addSubMenu(int group, int id, int categoryOrder, CharSequence title) {
      MenuItemImpl item = (MenuItemImpl)this.addInternal(group, id, categoryOrder, title);
      SubMenuBuilder subMenu = new NavigationSubMenu(mContext, this, item);
      item.setSubMenu(subMenu);
      return subMenu;
    }
  }

  @SuppressLint("RestrictedApi")
  public class NavigationSubMenu extends SubMenuBuilder {
    public NavigationSubMenu(Context context, NavigationMenu menu, MenuItemImpl item) {
      super(context, menu, item);
    }

    public void onItemsChanged(boolean structureChanged) {
      super.onItemsChanged(structureChanged);
      ((MenuBuilder)this.getParentMenu()).onItemsChanged(structureChanged);
    }
  }

}
