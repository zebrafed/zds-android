# Drawer example 2

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.drawer.ZebraNavigationView
    android:id="@+id/nav_view"
    android:layout_width="208dp"
    android:layout_height="match_parent"
    android:layout_gravity="start"
    android:background="@color/whiteColor"
    android:fitsSystemWindows="true"
    app:headerLayout="@layout/nav_header_drawer_demo"
    app:menu="@menu/activity_drawer_demo_drawer">

</com.zebra.Zebra.drawer.ZebraNavigationView>
```
```xml
<!-- @layout/nav_header_drawer_demo -->
<com.zebra.Zebra.drawer.ZebraDrawerHeader 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="bottom"
    android:orientation="vertical"
    android:paddingBottom="0dp">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.zebra.Zebra.drawer.ZebraAvatar
            android:id="@+id/imageView4"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:contentDescription="@string/nav_header_desc"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:srcCompat="@mipmap/ic_launcher_round" />

        <com.zebra.Zebra.drawer.ZebraAvatar
            android:id="@+id/imageView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:contentDescription="@string/nav_header_desc"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:size="small"
            app:srcCompat="@mipmap/ic_launcher_round" />
    </androidx.constraintlayout.widget.ConstraintLayout>

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@null"
        app:subheader="Details"
        app:subheaderTextAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        app:title="Name Surname"
        app:titleTextAppearance="@style/Widget_Zebra_Drawer.TitleAppearance" />

</com.zebra.Zebra.drawer.ZebraDrawerHeader>
```
```xml
<!-- @menu/activity_drawer_demo_drawer -->
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    >
    <item
        android:id="@+id/nav_account"
        android:icon="@drawable/person"
        android:title="@string/account" />
    <item
        android:id="@+id/nav_privacy"
        android:icon="@drawable/hand"
        android:title="@string/privacy" />
    <item
        android:id="@+id/nav_favorites"
        android:icon="@drawable/star"
        android:title="@string/favorites" />
    <item
        android:id="@+id/nav_documents"
        android:icon="@drawable/documents"
        android:title="@string/documents" />
    <item
        android:id="@+id/nav_cloud"
        android:icon="@drawable/cloudy"
        android:title="@string/cloud" />
    <item
        android:id="@+id/nav_settings"
        android:icon="@drawable/settings"
        android:title="@string/settings" />
</menu>
```

# Portable Drawer Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.drawer.ZebraNavigationView
    android:id="@+id/nav_view"
    android:layout_width="wrap_content"
    android:layout_height="match_parent"
    android:layout_gravity="start"
    android:background="@color/whiteColor"
    android:fitsSystemWindows="true"
    app:portable="true"
    app:headerLayout="@layout/nav_header_small_drawer_demo"
    app:menu="@menu/activity_drawer_demo_drawer">

</com.zebra.Zebra.drawer.ZebraNavigationView>
```
```xml
<!-- @layout/nav_header_small_drawer_demo -->
<com.zebra.Zebra.drawer.ZebraDrawerHeader xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="bottom"
    android:orientation="vertical" >

    <com.zebra.Zebra.drawer.ZebraAvatar
        android:id="@+id/imageView4"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        android:contentDescription="@string/nav_header_desc"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@mipmap/ic_launcher_round" />

</com.zebra.Zebra.drawer.ZebraDrawerHeader>
```
```xml
<!-- @menu/activity_drawer_demo_drawer -->
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    >
    <item
        android:id="@+id/nav_account"
        android:icon="@drawable/person"
        android:title="@string/account" />
    <item
        android:id="@+id/nav_privacy"
        android:icon="@drawable/hand"
        android:title="@string/privacy" />
    <item
        android:id="@+id/nav_favorites"
        android:icon="@drawable/star"
        android:title="@string/favorites" />
    <item
        android:id="@+id/nav_documents"
        android:icon="@drawable/documents"
        android:title="@string/documents" />
    <item
        android:id="@+id/nav_cloud"
        android:icon="@drawable/cloudy"
        android:title="@string/cloud" />
    <item
        android:id="@+id/nav_settings"
        android:icon="@drawable/settings"
        android:title="@string/settings" />
</menu>
```