package com.zebra.components.drawer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.zebra.components.R;
import com.zebra.components.Utils;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraAvatar extends AppCompatImageView {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Avatar;
  private final int mSize;

  private Path path;

  public ZebraAvatar(Context context) {
    this(context, null);
  }

  public ZebraAvatar(Context context, AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraAvatar);
  }

  public ZebraAvatar(Context context, AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraAvatar, defStyleAttr, DEF_STYLE_RES);

    mSize = attributes.getInt(R.styleable.ZebraAvatar_size, 48);

    init();

    attributes.recycle();
  }

  private void init() {
    path = new Path();
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    setMeasuredDimension(px(mSize), px(mSize));
  }

  private int px(int value) {
    return Utils.px(value, getContext());
  }

  @Override
  protected void onDraw(Canvas canvas) {
    path.addCircle(getWidth() / 2.0f, getHeight() / 2.0f, getWidth() / 2.0f, Path.Direction.CW);
    canvas.clipPath(path);
    super.onDraw(canvas);
  }
}