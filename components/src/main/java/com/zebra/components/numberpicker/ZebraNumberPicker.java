package com.zebra.components.numberpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Handler;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static java.lang.String.valueOf;

public class ZebraNumberPicker extends LinearLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Mini_NumberPicker;
    private static final String TAG = "com.zebra.components.numberpicker.ZebraNumberPicker";
    private ImageButton plus, minus;
    private EditText input;
    private int value, min, max;
    private boolean autoIncrement, autoDecrement;
    private Handler repeatUpdateHandler = new Handler();
    private static int REP_DELAY = 50;

    public ZebraNumberPicker(Context context) {
        this(context, null);
    }

    public ZebraNumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraNumberPicker);
    }

    public ZebraNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraNumberPicker, defStyleAttr, DEF_STYLE_RES);

        View theView = inflate(context, R.layout.number_picker, this);

        plus = theView.findViewById(R.id.plus);
        minus = theView.findViewById(R.id.minus);
        input = theView.findViewById(R.id.input);

        plus.setImageTintList(context.getColorStateList(R.color.icon_button));
        minus.setImageTintList(context.getColorStateList(R.color.icon_button));

        min = attributes.getInteger(R.styleable.ZebraNumberPicker_min, 0);
        max = attributes.getInteger(R.styleable.ZebraNumberPicker_max, 1000);

        if(min > max) {
            int i = max;
            max = min;
            min = i;
        } else if (max == min)  throw new IllegalArgumentException("Max and min can't be the same.");

        input.setFilters(new InputFilter[]{new InputFilterMinMax(min, max)});
        setValue(attributes.getInteger(R.styleable.ZebraNumberPicker_android_value, 0));
        attributes.recycle();

        setMinus();
        setPlus();
    }

    private void setPlus() {
        plus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                autoIncrement = true;
                repeatUpdateHandler.post(new ZebraNumberPicker.hRptUpdater());
                return false;
            }
        });

        plus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setValue(value + 1);
                }
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && autoIncrement) {
                    autoIncrement = false;
                }
                return false;
            }
        });
    }

    private void setMinus() {
        minus.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                autoDecrement = true;
                repeatUpdateHandler.post(new ZebraNumberPicker.hRptUpdater());
                return false;
            }
        });

        minus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setValue(value - 1);
                } else if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && autoDecrement) {
                    autoDecrement = false;
                }
                return false;
            }
        });
    }

    /**
     * This allows for pressing and holding to increase the values
     */
    private class hRptUpdater implements Runnable {
        public void run() {
            if (autoIncrement) {
                setValue(value + 1);
                repeatUpdateHandler.postDelayed(new ZebraNumberPicker.hRptUpdater(), REP_DELAY);
            } else if (autoDecrement) {
                setValue(value - 1);
                repeatUpdateHandler.postDelayed(new ZebraNumberPicker.hRptUpdater(), REP_DELAY);
            }
        }
    }

    /**
     * This ensures that the correct values are drawn to screen
     */
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (input.getText() == null || input.getText().toString().equals("")) setValue(min);
        if (value != Integer.parseInt(input.getText().toString()))
            setValue(Integer.parseInt(input.getText().toString()));
    }

    public int getValue() {
        value = Integer.parseInt(input.getText().toString());
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        if (value >= max) {
            value = max;
            autoIncrement = false;
            plus.setEnabled(false);
        } else {
            plus.setEnabled(true);
        }
        if (value <= min) {
            autoDecrement = false;
            value = min;
            minus.setEnabled(false);
        } else {
            minus.setEnabled(true);
        }
        input.setText(valueOf(value));
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    /**
     * Custom input filter allows only values in the min-max range to be entered
     */
    private class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }


        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) {
            }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}
