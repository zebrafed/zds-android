package com.zebra.components.header;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.appbar.AppBarLayout;

public class ZebraHeaderLayout extends AppBarLayout {
    public ZebraHeaderLayout(Context context) {
        this(context, null);
    }

    public ZebraHeaderLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setElevation(0f);
        setStateListAnimator(null);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        bringToFront();
    }
}
