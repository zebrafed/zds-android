# Zebra Header example

Header that can be used as the overall app support bar or as a toolbar.

<!--TODO change to include zebra header layout -->
```xml
<com.zebra.components.header.ZebraHeader
    android:id="@+id/toolbar"
    android:layout_width="match_parent"
    app:title="Page title"
    android:layout_height="wrap_content" />
```

This is set as the App bar within the Java class.

```java
toolbar = findViewById(R.id.toolbar);
setSupportActionBar(toolbar);
getSupportActionBar().setTitle("Page title");
```
To enable use of a navigation drawer:
```java
DrawerLayout drawer = findViewById(R.id.drawer_layout);
NavigationView navigationView = findViewById(R.id.nav_view);
ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
drawer.addDrawerListener(toggle);
toggle.syncState();
navigationView.setNavigationItemSelectedListener(this);
DemoPagerAdapter mViewPageAdapter = new DemoPagerAdapter(getSupportFragmentManager())
```

