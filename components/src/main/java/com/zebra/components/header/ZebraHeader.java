package com.zebra.components.header;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.VectorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.zebra.components.R;

import java.lang.reflect.Field;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;

public class ZebraHeader extends Toolbar {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Header;
    private static final String TAG = "com.zebra.components.header.ZebraHeader";
    private TextView titleTextView;
    private AppBarLayout.LayoutParams lps;
    private int backgroundColor, height;
    private LayerDrawable background;

    public ZebraHeader(@NonNull Context context) {
        this(context, null);
    }

    public ZebraHeader(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraHeader);
    }

    //TODO fix overflow icon size
    public ZebraHeader(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraHeader, defStyleAttr, DEF_STYLE_RES);
        backgroundColor = attributes.getColor(R.styleable.ZebraHeader_android_colorBackground, context.getColor(R.color.primaryColor));
        background = (LayerDrawable) context.getDrawable(R.drawable.header_background);
        height = px(48, context);
        if (Mini(context)) {
            height = px(56, context);
        }
        height = attributes.getDimensionPixelOffset(R.styleable.ZebraHeader_android_height, height);
        attributes.recycle();
        applyAttrs();
        VectorDrawable d = (VectorDrawable) getOverflowIcon();
        int i = px(0, context);

//        Field f = ActionMenuView.class.getDeclaredField("mNavButtonView");
        d.setBounds(i, i, i, i);
        setOverflowIcon(d);
    }

    @Override
    public void setOverflowIcon(@Nullable Drawable icon) {
        icon.setBounds(0, 0, 12, 12);
        //todo make the overflow icon smaller somehow...
        super.setOverflowIcon(icon);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (getHeight() != height) {
            setLayoutParams(lps);
        }
    }

    private void applyAttrs() {
        if (background != null) {
            background.findDrawableByLayerId(R.id.back).setTint(backgroundColor);
            setBackground(background);
        } else {
            setBackgroundColor(backgroundColor);
        }
        lps = new AppBarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        setLayoutParams(lps);
    }

    private void setTitleTextView(Context context) {
        Toolbar.LayoutParams lp = new Toolbar.LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;

        titleTextView = new TextView(context);
        if (Mini(context)) {
            titleTextView.setTextAppearance(R.style.Widget_Mini_Header_Text);
        } else {
            titleTextView.setTextAppearance(R.style.Widget_Zebra_Header_Text);
        }
        setTitle("");
        titleTextView.setEllipsize(TextUtils.TruncateAt.END);
        titleTextView.setHorizontallyScrolling(false);
        titleTextView.setSingleLine();

        titleTextView.setLayoutParams(lp);
        addView(titleTextView);
    }

    @Override
    public void setTitle(int resId) {
        super.setTitle(resId);
        super.setTitle("");
        if (titleTextView == null) {
            setTitleTextView(getContext());
        }
        titleTextView.setText(resId);
    }

    @Override
    public void setTitle(CharSequence text) {
        super.setTitle(text);
        super.setTitle("");
        if (titleTextView == null) {
            setTitleTextView(getContext());
        }
        titleTextView.setText(text);
    }
}
