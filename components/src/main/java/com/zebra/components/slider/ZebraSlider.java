package com.zebra.components.slider;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatSeekBar;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;
import static com.zebra.components.Utils.px;

public class ZebraSlider extends LinearLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Slider;
    private static final String TAG = "com.zebra.components.slider.ZebraSlider";
    private ImageView startIcon, endIcon;
    private int color, progressBackgroundColor, iconSize, padding, progress, min, max, colorBackground;
    private LayerDrawable track;
    private AppCompatSeekBar slider;
    private Drawable thumb, startIconDrawable, endIconDrawable;
    private boolean isStepped;
    private ViewGroup.LayoutParams sliderLPS;

    public ZebraSlider(Context context) {
        super(context);
        getAttrs(context, null, R.attr.ZebraSlider);
    }

    public ZebraSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        getAttrs(context, attrs, R.attr.ZebraSlider);
    }

    public ZebraSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        getAttrs(context, attrs, defStyleAttr);
    }

    private void getAttrs(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraSlider, defStyleAttr, DEF_STYLE_RES);

        slider = new AppCompatSeekBar(context);
        padding = px(8, context);
        sliderLPS = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f);

        setTrack((LayerDrawable) context.getDrawable(R.drawable.slider_drawable_progress));
        setThumb(attributes.getDrawable(R.styleable.ZebraSlider_android_thumb));

        isStepped = attributes.getBoolean(R.styleable.ZebraSlider_stepped, false);
        startIconDrawable = attributes.getDrawable(R.styleable.ZebraSlider_startIcon);
        endIconDrawable = attributes.getDrawable(R.styleable.ZebraSlider_endIcon);
        iconSize = attributes.getDimensionPixelSize(R.styleable.ZebraSlider_iconSize, px(32, getContext())) + (2 * padding);
        color = attributes.getColor(R.styleable.ZebraSlider_android_color, context.getColor(R.color.primaryColor));
        progressBackgroundColor = attributes.getColor(R.styleable.ZebraSlider_android_progressBackgroundTint, context.getColor(R.color.gray_100));
        colorBackground = attributes.getColor(R.styleable.ZebraSlider_android_colorBackground, context.getColor(R.color.white));
        progress = attributes.getInt(R.styleable.ZebraSlider_android_progress, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            min = attributes.getInt(R.styleable.ZebraSlider_android_min, 0);
        } else {
            min = 0;
        }

        if (isStepped) {
            max = attributes.getInt(R.styleable.ZebraSlider_android_max, 5);
        } else {
            max = attributes.getInt(R.styleable.ZebraSlider_android_max, 100);
        }
        attributes.recycle();
        applyAttrs(context);
    }

    private void makeIcons(Context context) {
        startIcon = new ImageView(context);
        endIcon = new ImageView(context);

        startIcon.setImageDrawable(startIconDrawable);
        endIcon.setImageDrawable(endIconDrawable);

        ViewGroup.LayoutParams lps = new ViewGroup.LayoutParams(iconSize, iconSize);
        startIcon.setPadding(padding, padding, 0, padding);
        endIcon.setPadding(0, padding, padding, padding);

        slider.setPadding(padding + px(2, context), 0, padding + px(2, context), 0);
        startIcon.setLayoutParams(lps);
        endIcon.setLayoutParams(lps);

        this.addView(startIcon);
        this.addView(slider);
        this.addView(endIcon);
    }

    private void applyAttrs(Context context) {
        track.mutate();
        thumb.mutate();

        slider.setSplitTrack(false);
        slider.setLayoutParams(sliderLPS);

        this.setGravity(Gravity.CENTER_VERTICAL);
        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setWeightSum(1);


        if (startIconDrawable != null && endIconDrawable != null) {
            makeIcons(context);
        } else {
            this.addView(slider);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            slider.setMin(min);
        }

        slider.setMax(max);
        setColor(color);
        setProgressBackgroundColor(progressBackgroundColor);
        slider.setProgress(progress);
    }

    public int getColor() {
        return color;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (isStepped) {
            Context context = getContext();
            Paint paint = new Paint();
            int total, i, distance, strokeWidth, numbLines;
            numbLines = getMax() - getMin() + 1;

            strokeWidth = px(2, context);
            paint.setColor(color);
            paint.setStrokeWidth((float) strokeWidth);
            total = slider.getWidth() - (slider.getPaddingStart() + slider.getPaddingEnd());
            distance = ((total - ((numbLines - 1) * strokeWidth)) / (numbLines - 1));

            if (startIconDrawable != null && endIconDrawable != null) {
                i = slider.getPaddingStart() + startIcon.getWidth();
            } else {
                i = slider.getPaddingStart();
            }

            drawLines(canvas, paint, i, distance, strokeWidth);
        }
    }

    private void drawLines(Canvas canvas, Paint paint, int i, int distance, int strokeWidth) {
        int startY = (slider.getMeasuredHeight() / 2) - (thumb.getIntrinsicHeight() / 4);
        int endY = startY + px(12, getContext());
        int lines = 0;
        while (lines < (getMax() - getMin() + 1)) {
            canvas.drawLine(i, startY, i, endY, paint);
            i += strokeWidth + distance;
            lines++;
        }
    }

    public void setColor(int color) {
        this.color = color;
        applyColors();
    }

    private void applyColors() {
        track.findDrawableByLayerId(R.id.progress).setTint(color);
        thumb.setTint(color);
        slider.getThumb().setTint(color);
        setBackgroundColor(colorBackground);
        if (startIconDrawable != null && endIconDrawable != null) {
            startIcon.setImageTintList(ColorStateList.valueOf(color));
            endIcon.setImageTintList(ColorStateList.valueOf(color));
        }
    }

    public int getProgressBackgroundColor() {
        return progressBackgroundColor;
    }

    public void setProgressBackgroundColor(int progressBackgroundColor) {
        this.progressBackgroundColor = progressBackgroundColor;
        track.findDrawableByLayerId(R.id.background).setTint(progressBackgroundColor);
    }

    public int getProgress() {
        return slider.getProgress();
    }

    public void setProgress(int progress) {
        slider.setProgress(progress);
    }

    public int getMin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return slider.getMin();
        }
        return 0;
    }

    public void setMin(int min) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            slider.setMin(min);
        }
    }

    public int getMax() {
        return slider.getMax();
    }

    public void setMax(int max) {
        slider.setMax(max);
    }

    public int getIconSize() {
        return iconSize;
    }

    public void setIconSize(int iconSize) {
        this.iconSize = iconSize;
        makeIcons(getContext());
    }

    public LayerDrawable getTrack() {
        return track;
    }

    public void setTrack(LayerDrawable track) {
        this.track = track;
        slider.setProgressDrawable(track);
    }

    public Drawable getThumb() {
        return thumb;
    }

    public void setThumb(Drawable thumb) {
        this.thumb = thumb;
        slider.setThumb(thumb);
    }

    public Drawable getStartIconDrawable() {
        return startIconDrawable;
    }

    public void setStartIconDrawable(Drawable startIconDrawable) {
        this.startIconDrawable = startIconDrawable;
        makeIcons(getContext());
    }

    public Drawable getEndIconDrawable() {
        return endIconDrawable;
    }

    public void setEndIconDrawable(Drawable endIconDrawable) {
        this.endIconDrawable = endIconDrawable;
        makeIcons(getContext());
    }

    public boolean isStepped() {
        return isStepped;
    }

    public void setStepped(boolean stepped) {
        isStepped = stepped;
    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);
        colorBackground = color;
    }
}