# Sliders
Sliders allow users to make selections from a range of values.

# Zebra slider default example
```xml
 <com.zebra.components.slider.ZebraSlider
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:max="100"
    android:min="0"
    android:progress="10" />
```

# Zebra Slider with icons example
```xml
 <com.zebra.components.slider.ZebraSlider
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:max="100"
    android:min="0"
    android:progress="10"
    app:endIcon="@drawable/brightnessup"
    app:startIcon="@drawable/brightnessdown"/>
```

# Zebra Slider stepped example
No more than 10 steps can be had
```xml
 <com.zebra.components.slider.ZebraSlider
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:max="5"
    android:min="0"
    android:progress="10"
    app:stepped="true"/>
```

# Zebra Slider stepped with icons example
No more than 10 steps can be had
```xml
 <com.zebra.components.slider.ZebraSlider
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:max="10"
    android:min="0"
    android:progress="10"
    app:endIcon="@drawable/brightnessup"
    app:startIcon="@drawable/brightnessdown"
    app:stepped="true"/>
```