# Progress indicators

# Horizontal Progress indicator example
```xml
<com.zebra.components.prograssbar.ZebraProgressBar
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:layout_marginVertical="24dp"
    android:progress="50" />
```
                  
# Circular progress indicator example
```xml
<com.zebra.components.prograssbar.ZebraProgressBar
    style="@style/Widget_Zebra_ProgressBar.Circle"
    android:layout_width="match_parent"
    android:layout_height="48dp"
    android:layout_marginVertical="24dp"
    android:progress="50" />
```
