package com.zebra.components.prograssbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.zebra.components.R;
import static com.zebra.components.Utils.createThemedContext;

public class ZebraProgressBar extends ProgressBar {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_ProgressBar;


    public ZebraProgressBar(Context context) {
        this(context, null);
    }

    public ZebraProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraProgressBar);
    }

    public ZebraProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

//        TypedArray attributes = obtainStyledAttributes(context, attrs, R.styleable.ZebraProgressBar, defStyleAttr, DEF_STYLE_RES);
//        attributes.recycle();
    }
}
