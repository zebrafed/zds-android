package com.zebra.components.action;

import com.zebra.components.R;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import static com.zebra.components.Utils.createThemedContext;

public class ZebraAction extends AppCompatButton {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Action;

  public ZebraAction(Context context) {
    this(context, null);
  }

  public ZebraAction(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraAction);
  }

  public ZebraAction(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
  }
}
