# Zebra Top Tabs example

The colors on the headers are linked to another Sketch library and can be overridden. 
```xml
<com.zebra.components.tabs.ZebraTabLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:marginTop="58dp">

    <com.zebra.components.tabs.ZebraTabItem
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="item 1" />

    <com.zebra.components.tabs.ZebraTabItem
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="item 2" />

    <com.zebra.components.tabs.ZebraTabItem
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Item 3" />
</com.zebra.components.tabs.ZebraTabLayout>
```