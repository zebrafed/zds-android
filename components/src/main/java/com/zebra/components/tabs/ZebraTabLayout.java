package com.zebra.components.tabs;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;

import com.google.android.material.tabs.TabLayout;
import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraTabLayout extends TabLayout {
    private static final int DEF_STYLE_RES = R.style.Widget_Zebra_TabLayout;
    private LayerDrawable background;
    private int backgroundColor;

    public ZebraTabLayout(Context context) {
        this(context, null);
    }

    public ZebraTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.ZebraTabLayout);
    }

    public ZebraTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraHeader, defStyleAttr, DEF_STYLE_RES);
        backgroundColor = attributes.getColor(R.styleable.ZebraTabLayout_android_colorBackground, context.getColor(R.color.primaryColor));
        setBackground(context.getDrawable(R.drawable.tablayout_background));
        attributes.recycle();
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        applyBackgroundColor();
    }

    private void applyBackgroundColor() {
        if (background != null) {
            if (background.findDrawableByLayerId(R.id.back) != null) {
                background.findDrawableByLayerId(R.id.back).setTint(backgroundColor);
            }
        }
    }

    @Override
    public void setBackground(Drawable background) {
        super.setBackground(background);
        if (background instanceof LayerDrawable) {
            this.background = (LayerDrawable) background;
        }
        applyBackgroundColor();
    }
}
