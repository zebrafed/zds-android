package com.zebra.components.tabs;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.material.tabs.TabItem;

public class ZebraTabItem extends TabItem {
    public ZebraTabItem(Context context) {
        this(context, null);
    }

    public ZebraTabItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
