package com.zebra.components.ripple;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageButton;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraRipple extends AppCompatImageButton {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Action;
  private int mTintColor = -1;

  public ZebraRipple(Context context) {
     this(context, null);
  }

  public ZebraRipple(Context context, AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraRipple);
  }

  public ZebraRipple(Context context, AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraRipple, defStyleAttr, DEF_STYLE_RES);

    if (attributes.hasValue(R.styleable.ZebraRipple_active)) {
      boolean isActive = attributes.getBoolean(R.styleable.ZebraRipple_active, false);
      setActive(isActive);
    }

    attributes.recycle();
  }

  public void setActive(boolean isActive) {
    if (isActive) {
      setBackgroundResource(R.drawable.ripple_background_primary);
    }else {
      setBackgroundResource(R.drawable.ripple_background_secondary);
    }
  }

  public void setColor(int color) {
    mTintColor = color;
    setImageTintList(new ColorStateList(new int[][]{ new int[]{} }, new int[]{ color }));
  }
}
