package com.zebra.components.card;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zebra.components.R;
import com.zebra.components.Utils;

import static androidx.appcompat.widget.ListPopupWindow.MATCH_PARENT;
import static androidx.appcompat.widget.ListPopupWindow.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraCardHeaderView extends FrameLayout {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_CardHeader;
  private final int mIconSize;
  private final int mIconOffset;
  private int mTitleTextAppearance;
  private final int mSubheaderTextAppearance;
  private final int mIconColor;
  private final int mInfoTextAppearance;
  private FrameLayout contentLayout;
  private int mLeftOffset = 10;
  private String mTitle;
  private String mSubHeader;
  private String mInfoHeader;
  private TextView mTitleTextView;
  private TextView mSubheaderTextView;
  private TextView mInfoHeaderTextView;
  private ImageView mIcon;
  private int minHeight;

  public ZebraCardHeaderView(@NonNull Context context) {
    this(context, null /* attrs */);
  }

  public ZebraCardHeaderView(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraCardHeaderView);
  }

  public ZebraCardHeaderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraCardHeader, defStyleAttr, DEF_STYLE_RES);

    minHeight = attributes.getDimensionPixelSize(R.styleable.ZebraCardHeader_android_minHeight, 0);

    contentLayout = new FrameLayout(context);
    LayoutParams lp = new LayoutParams(MATCH_PARENT, minHeight);
    lp.setMargins(0,0,0,0);
    contentLayout.setLayoutParams(lp);

    mIconSize = attributes.getDimensionPixelSize(R.styleable.ZebraCardHeader_iconSize, 24);
    mIconOffset = attributes.getDimensionPixelSize(R.styleable.ZebraCardHeader_iconOffset, 6);
    mTitleTextAppearance = attributes.getResourceId(R.styleable.ZebraCardHeader_titleTextAppearance, R.style.Widget_Zebra_Card_TitleAppearance);
    mSubheaderTextAppearance = attributes.getResourceId(R.styleable.ZebraCardHeader_subheaderTextAppearance, R.style.Widget_Zebra_Card_SubheaderAppearance);
    mInfoTextAppearance = attributes.getResourceId(R.styleable.ZebraCardHeader_infoTextAppearance, R.style.Widget_Zebra_Card_InfoAppearance);

    mTitle = attributes.getString(R.styleable.ZebraCardHeader_title);
    mSubHeader = attributes.getString(R.styleable.ZebraCardHeader_subheader);
    mInfoHeader = attributes.getString(R.styleable.ZebraCardHeader_info);
    Drawable drawable = attributes.getDrawable(R.styleable.ZebraCardHeader_icon);
    mIconColor = attributes.getColor(R.styleable.ZebraCardHeader_icon_color, android.R.attr.colorPrimary);

    mLeftOffset = attributes.getDimensionPixelSize(R.styleable.ZebraCardHeader_android_paddingStart, 0);

    setPadding(0,0,0,0);

    super.addView(contentLayout, -1, lp);

    if (drawable != null) {
      setIcon(drawable);
    }

    if (mTitle != null && !mTitle.isEmpty()) {
      setTitle(mTitle, mTitleTextAppearance);
    }

    if (mSubHeader != null && !mSubHeader.isEmpty()) {
      setSubHeader(mSubHeader, mSubheaderTextAppearance);
    }

    if (mInfoHeader != null && !mInfoHeader.isEmpty()) {
      setInfoHeader(mInfoHeader, mInfoTextAppearance);
    }

    attributes.recycle();
  }

  public void setIcon(Drawable drawable) {
    this.setIcon(drawable, mIconColor);
  }

  public void setIcon(Drawable drawable, int color) {
    drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    mIcon = new ImageView(getContext());
    mIcon.setImageDrawable(drawable);
    LayoutParams iconLayout = new LayoutParams(mIconSize, mIconSize);
    iconLayout.gravity = Gravity.CENTER_VERTICAL;
    iconLayout.setMargins(mIconOffset, 0, 0, 0);
    contentLayout.addView(mIcon, iconLayout);
    mLeftOffset = mIconSize + mIconOffset + mIconOffset;
  }

  public void setTitle(String text) {
    this.setTitle(text, mTitleTextAppearance);
  }

  public void setTitle(String text, int textAppearance) {
    mTitleTextAppearance = textAppearance;

    if (mTitleTextView != null) {
      mTitleTextView.setText(text);
      mTitleTextView.setTextAppearance(getContext(), textAppearance);
      return;
    }

    mTitleTextView = new TextView(getContext());
    mTitleTextView.setText(text);
    mTitleTextView.setEllipsize(TextUtils.TruncateAt.END);
    mTitleTextView.setTextAppearance(getContext(), textAppearance);
    mTitleTextView.setPadding(mLeftOffset, 0, px(10), 0);
    LayoutParams titleLayout = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
    titleLayout.gravity = Gravity.CENTER_VERTICAL;
    contentLayout.addView(mTitleTextView, titleLayout);
  }

  public void setSubHeader(String text) {
    this.setSubHeader(text, mSubheaderTextAppearance);
  }

  public void setSubHeader(String text, int textAppearance) {
    if (mSubheaderTextView != null) {
      mSubheaderTextView.setText(text);
      mSubheaderTextView.setTextAppearance(getContext(), textAppearance);
      return;
    }

    TypedArray padding = getContext().obtainStyledAttributes(textAppearance, new int[] { android.R.attr.paddingTop, android.R.attr.paddingBottom });
    TypedArray titlePadding = getContext().obtainStyledAttributes(mTitleTextAppearance, new int[] { android.R.attr.paddingTop, android.R.attr.paddingBottom });

    int paddingTop = padding.getDimensionPixelSize(0, 0);
    int paddingBottom = padding.getDimensionPixelSize(1, 0);
    int titlePaddingTop = titlePadding.getDimensionPixelSize(0, 0);
    int titlePaddingBottom = titlePadding.getDimensionPixelSize(1, 0);

    mSubheaderTextView = new TextView(getContext());
    mSubheaderTextView.setText(text);
    mSubheaderTextView.setEllipsize(TextUtils.TruncateAt.END);
    mSubheaderTextView.setTextAppearance(getContext(), textAppearance);

    LinearLayout.LayoutParams subheaderLayout = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
    mSubheaderTextView.setPadding(mLeftOffset, paddingTop, px(10), paddingBottom);

    mTitleTextView.setPadding(mLeftOffset, titlePaddingTop, px(10), titlePaddingBottom);
    LayoutParams titleLayout = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
    titleLayout.gravity = Gravity.TOP;
    mTitleTextView.setLayoutParams(titleLayout);

    contentLayout.addView(mSubheaderTextView, subheaderLayout);

    padding.recycle();
    titlePadding.recycle();
  }

  public void setInfoHeader(String text) {
    this.setInfoHeader(text, mInfoTextAppearance);
  }

  public void setInfoHeader(String text, int textAppearance) {
    if (mInfoHeaderTextView != null) {
      mInfoHeaderTextView.setText(text);
      mInfoHeaderTextView.setTextAppearance(getContext(), textAppearance);
      return;
    }

    TypedArray padding = getContext().obtainStyledAttributes(textAppearance, new int[] { android.R.attr.paddingTop, android.R.attr.paddingBottom });

    int paddingTop = padding.getDimensionPixelSize(0, 0);
    int paddingBottom = padding.getDimensionPixelSize(1, 0);

    mInfoHeaderTextView = new TextView(getContext());
    mInfoHeaderTextView.setText(text);
    mInfoHeaderTextView.setEllipsize(TextUtils.TruncateAt.END);
    mInfoHeaderTextView.setTextAppearance(getContext(), textAppearance);

    LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
    mInfoHeaderTextView.setPadding(mLeftOffset, paddingTop, px(10), paddingBottom);

    contentLayout.addView(mInfoHeaderTextView, layout);

    LayoutParams _lp = new LayoutParams(MATCH_PARENT, minHeight + px(24));
    _lp.setMargins(0,0,0,0);
    contentLayout.setLayoutParams(_lp);

    LayoutParams iconLayout = new LayoutParams(mIconSize, mIconSize);
    iconLayout.gravity = Gravity.START;
    iconLayout.setMargins(mIconOffset, px(20), 0, 0);

    mIcon.setLayoutParams(iconLayout);

    padding.recycle();
  }


  private int px(int value) {
    return Utils.px(value, getContext());
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    contentLayout.addView(child, index, params);
  }

  @Override
  public void removeAllViews() {
    contentLayout.removeAllViews();
  }

  @Override
  public void removeView(View view) {
    contentLayout.removeView(view);
  }

  @Override
  public void removeViewInLayout(View view) {
    contentLayout.removeViewInLayout(view);
  }

  @Override
  public void removeViewsInLayout(int start, int count) {
    contentLayout.removeViewsInLayout(start, count);
  }

  @Override
  public void removeViewAt(int index) {
    contentLayout.removeViewAt(index);
  }

  @Override
  public void removeViews(int start, int count) {
    contentLayout.removeViews(start, count);
  }
}
