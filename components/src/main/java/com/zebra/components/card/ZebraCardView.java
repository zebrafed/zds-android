package com.zebra.components.card;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.zebra.components.R;

import static android.widget.LinearLayout.VERTICAL;
import static androidx.appcompat.widget.ListPopupWindow.MATCH_PARENT;
import static com.zebra.components.Utils.createThemedContext;

public class ZebraCardView extends CardView {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_Card;
  private LinearLayout contentLayout;

  public ZebraCardView(@NonNull Context context) {
    this(context, null /* attrs */);
  }

  public ZebraCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraCardView);
  }

  public ZebraCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
//TODO: fix this whole thing...
    contentLayout = new LinearLayout(context);
    contentLayout.setOrientation(VERTICAL);
    LayoutParams lp = new LayoutParams(MATCH_PARENT, MATCH_PARENT);
    contentLayout.setLayoutParams(lp);

    super.addView(contentLayout, -1, lp);
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    contentLayout.addView(child, index, params);
  }

  @Override
  public void addView(View child) {
    contentLayout.addView(child);
  }

  @Override
  public void addView(View child, ViewGroup.LayoutParams params) {
    contentLayout.addView(child, params);
  }

  @Override
  public void removeAllViews() {
    contentLayout.removeAllViews();
  }

  @Override
  public void removeView(View view) {
    contentLayout.removeView(view);
  }

  @Override
  public void removeViewInLayout(View view) {
    contentLayout.removeViewInLayout(view);
  }

  @Override
  public void removeViewsInLayout(int start, int count) {
    contentLayout.removeViewsInLayout(start, count);
  }

  @Override
  public void removeViewAt(int index) {
    contentLayout.removeViewAt(index);
  }

  @Override
  public void removeViews(int start, int count) {
    contentLayout.removeViews(start, count);
  }

  @Override
  public void setLayoutParams(ViewGroup.LayoutParams params) {
    super.setLayoutParams(params);
    LayoutParams layoutParams = (LayoutParams) contentLayout.getLayoutParams();
    if (params instanceof LayoutParams) {
      layoutParams.gravity = ((LayoutParams) params).gravity;
      contentLayout.requestLayout();
    }
  }
}
