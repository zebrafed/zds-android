package com.zebra.components.card;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zebra.components.R;

import static androidx.appcompat.widget.ListPopupWindow.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraCardActions extends LinearLayout {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_CardActions;
  private final float mWeight;
  private final int mInnerSpace;

  public ZebraCardActions(@NonNull Context context) {
    this(context, null /* attrs */);
  }

  public ZebraCardActions(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraCardActions);
  }

  public ZebraCardActions(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraCardActions, defStyleAttr, DEF_STYLE_RES);

    int paddingBottom = attributes.getDimensionPixelSize(R.styleable.ZebraCardActions_android_paddingBottom, 0);
    int paddingTop = attributes.getDimensionPixelSize(R.styleable.ZebraCardActions_android_paddingTop, 0);
    mWeight = attributes.getFloat(R.styleable.ZebraCardActions_android_layout_weight, 0.0f);
    mInnerSpace = attributes.getDimensionPixelSize(R.styleable.ZebraCardActions_innerSpace, 0);

    setOrientation(LinearLayout.HORIZONTAL);
    setPadding(0, paddingTop, 0, paddingBottom);
    setGravity(Gravity.END);

    attributes.recycle();
  }

  @Override
  public void addView(View child, int index, ViewGroup.LayoutParams params) {
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, mWeight);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    super.addView(child, index, lp);
  }

  @Override
  public void addView(View child) {
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, mWeight);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    child.setLayoutParams(lp);
    super.addView(child);
  }

  @Override
  public void addView(View child, ViewGroup.LayoutParams params) {
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, mWeight);
    lp.setMarginStart(mInnerSpace);
    lp.setMarginEnd(mInnerSpace);
    super.addView(child, lp);
  }
}
