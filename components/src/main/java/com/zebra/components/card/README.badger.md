# Default Card Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:id="@+id/ZebraCardView"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/pin"
        app:subheader="Details"
        app:title="List item" />

    <TextView
        style="@style/ZebraCardTextAppearance"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam." />

</com.zebra.Zebra.card.ZebraCardView>
```

# Card with actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/pin"
        app:subheader="Details"
        app:title="List item" />

    <TextView
        style="@style/ZebraCardTextAppearance"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam." />

    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Card with media

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
 <com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/pin"
        app:subheader="Details"
        app:title="List item">

    </com.zebra.Zebra.card.ZebraCardHeaderView>

    <ImageView
        android:id="@+id/imageView"
        android:layout_width="match_parent"
        android:layout_height="210dp"
        android:adjustViewBounds="false"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

</com.zebra.Zebra.card.ZebraCardView>
```

# Card with media and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/pin"
        app:subheader="Details"
        app:title="List item">

    </com.zebra.Zebra.card.ZebraCardHeaderView>

    <ImageView
        android:id="@+id/imageView1"
        android:layout_width="match_parent"
        android:layout_height="97dp"
        android:adjustViewBounds="false"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button3"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button4"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Card with different, content and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <LinearLayout
        style="@style/ZebraCardTextAppearance"
        android:orientation="vertical"
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:paddingBottom="5dp"
            android:text="End detection timeout (sec):"
            android:textColor="@color/graySwatches_900"
            android:textSize="16sp"
            android:textStyle="bold" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="Zero is a special value. If “End phrase” is defined, it is infinite, else, data is returned immediately."
            android:textColor="@color/graySwatches_700" />

    </LinearLayout>

    <ImageView
        android:id="@+id/imageView3"
        android:layout_width="match_parent"
        android:layout_height="48dp"
        android:adjustViewBounds="false"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button5"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="@android:string/cancel" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button6"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@android:string/ok" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Swipeable card example with one action

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraSwipeable
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_marginBottom="16dp"
    app:dragEdge="right">

    <Button
        android:id="@+id/button7"
        style="@style/Widget_Zebra_Swipeable_Action"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:text="Test audio" />

    <include layout="@layout/swipable_item_content" />
     
</com.zebra.Zebra.card.ZebraSwipeable>
```
```xml
<!-- @layout/swipable_item_content -->
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:paddingEnd="8dp">

    <ImageView
        android:id="@+id/imageView5"
        android:layout_width="48dp"
        android:layout_height="48dp"
        android:layout_marginStart="4dp"
        android:layout_marginTop="4dp"
        android:layout_marginBottom="4dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/icon_placeholder" />

    <ImageView
        android:id="@+id/imageView6"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginTop="6dp"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/ic_circle" />

    <TextView
        android:id="@+id/textView4"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="8dp"
        android:text="HS3100"
        android:textAppearance="@style/Widget_Zebra_Card.TitleAppearance"
        android:textStyle="bold"
        app:layout_constraintEnd_toStartOf="@+id/textView7"
        app:layout_constraintStart_toEndOf="@+id/imageView6"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView5"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="4dp"
        android:text="Battery: 95%"
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        app:layout_constraintBottom_toTopOf="@+id/textView6"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toBottomOf="@+id/textView4" />

    <TextView
        android:id="@+id/textView7"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginTop="2dp"
        android:layout_marginEnd="8dp"
        android:text="14:47"
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        android:textColor="@color/secondaryColor_1"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/textView4"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView6"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="4dp"
        android:layout_marginBottom="4dp"
        android:text="Health: Normal   "
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toBottomOf="@+id/textView5" />
</androidx.constraintlayout.widget.ConstraintLayout>
```

# Swipeable card example with multiple action

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraSwipeable
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    app:dragEdge="right">

    <LinearLayout
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:orientation="horizontal">

        <Button
            style="@style/Widget_Zebra_Swipeable_Action"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:background="@color/dangerColor"
            android:text="Test audio" />

        <Button
            style="@style/Widget_Zebra_Swipeable_Action"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:text="Test audio" />
    </LinearLayout>

    <include layout="@layout/swipable_item_content" />
</com.zebra.Zebra.card.ZebraSwipeable>
```
```xml
<!-- @layout/swipable_item_content -->
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="#FFFFFF"
    android:paddingEnd="8dp">

    <ImageView
        android:id="@+id/imageView5"
        android:layout_width="48dp"
        android:layout_height="48dp"
        android:layout_marginStart="4dp"
        android:layout_marginTop="4dp"
        android:layout_marginBottom="4dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/icon_placeholder" />

    <ImageView
        android:id="@+id/imageView6"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginTop="6dp"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/ic_circle" />

    <TextView
        android:id="@+id/textView4"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="8dp"
        android:text="HS3100"
        android:textAppearance="@style/Widget_Zebra_Card.TitleAppearance"
        android:textStyle="bold"
        app:layout_constraintEnd_toStartOf="@+id/textView7"
        app:layout_constraintStart_toEndOf="@+id/imageView6"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView5"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="4dp"
        android:text="Battery: 95%"
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        app:layout_constraintBottom_toTopOf="@+id/textView6"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toBottomOf="@+id/textView4" />

    <TextView
        android:id="@+id/textView7"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginTop="2dp"
        android:layout_marginEnd="8dp"
        android:text="14:47"
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        android:textColor="@color/secondaryColor_1"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/textView4"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView6"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginEnd="4dp"
        android:layout_marginBottom="4dp"
        android:text="Health: Normal   "
        android:textAppearance="@style/Widget_Zebra_Drawer.SubheaderAppearance"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/imageView5"
        app:layout_constraintTop_toBottomOf="@+id/textView5" />
</androidx.constraintlayout.widget.ConstraintLayout>
```