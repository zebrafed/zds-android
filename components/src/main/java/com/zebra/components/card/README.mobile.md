# Mobile Card Example with tertiary information

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">


    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:info="Tertiary information"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example with actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">


    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>
    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:id="@+id/button2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example, tertiary information and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">


    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:info="Tertiary information"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>
    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example with media

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>

    <ImageView
        android:id="@+id/imageView7"
        android:layout_width="match_parent"
        android:layout_height="157dp"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example with media and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">

    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>

    <ImageView
        android:layout_width="match_parent"
        android:layout_height="157dp"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:paddingTop="10dp">

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example, tertiary info with media

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">


    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:info="Tertiary information"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>
    <ImageView
        android:layout_width="match_parent"
        android:layout_height="157dp"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Card Example, tertiary info with media and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraCardView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="16dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="16dp">


    <com.zebra.Zebra.card.ZebraCardHeaderView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:icon="@drawable/person"
        app:info="Tertiary information"
        app:subheader="Secondary information"
        app:title="Simple card">

        <ImageView
            android:layout_width="24dp"
            android:layout_height="24dp"
            android:layout_gravity="end"
            android:layout_marginTop="20dp"
            android:layout_marginEnd="8dp"
            android:tint="?attr/colorPrimary"
            app:srcCompat="@drawable/clock" />

    </com.zebra.Zebra.card.ZebraCardHeaderView>
    <ImageView
        android:layout_width="match_parent"
        android:layout_height="157dp"
        android:scaleType="centerCrop"
        app:srcCompat="@drawable/image_placeholder" />
    <com.zebra.Zebra.card.ZebraCardActions
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="right|center_vertical"
            android:text="Action 1" />

        <com.zebra.Zebra.action.ZebraAction
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Action 2" />
    </com.zebra.Zebra.card.ZebraCardActions>

</com.zebra.Zebra.card.ZebraCardView>
```

# Mobile Swipeable card example with one action

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.card.ZebraSwipeable
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_marginStart="8dp"
    android:layout_marginBottom="16dp"
    app:dragEdge="right">

    <ImageButton
        style="@style/Widget_ZebraMobile_Swipeable_Action"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:src="@drawable/delete_alt"
        android:text="Test audio"
        />

    <FrameLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.zebra.Zebra.card.ZebraCardHeaderView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginEnd="8dp"
            app:icon="@drawable/person"
            app:info="Tertiary information"
            app:subheader="Secondary information"
            app:title="Simple card">

            <ImageView
                android:layout_width="24dp"
                android:layout_height="24dp"
                android:layout_gravity="end"
                android:layout_marginTop="20dp"
                android:layout_marginEnd="8dp"
                android:tint="?attr/colorPrimary"
                app:srcCompat="@drawable/clock" />

        </com.zebra.Zebra.card.ZebraCardHeaderView>

    </FrameLayout>
</com.zebra.Zebra.card.ZebraSwipeable>
```