package com.zebra.components.search;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ContextThemeWrapper;

import com.zebra.components.R;
import com.zebra.components.Utils;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraSearch extends LinearLayout implements View.OnTouchListener, TextView.OnEditorActionListener, TextWatcher {
  private static final int DEF_STYLE_RES = R.style.Widget_Zebra_Search;
  private final EditText mTextField;
  private final Drawable mLeadingDrawable;
  private final int mEndPaddng;
  private OnSubmitListener mOnSubmitListener;
  private OnChangeListener mOnChangeListener;
  private OnFocusChangeListener mOnFocusChangeListener;
  private ZebraSearch.PassThroughHierarchyChangeListener mPassThroughListener;

  public ZebraSearch(@NonNull Context context) {
    this(context, null);
  }

  public ZebraSearch(@NonNull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraSearch);
  }

  public ZebraSearch(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraSearch, defStyleAttr, DEF_STYLE_RES);

    int textFieldStyles = attributes.getResourceId(R.styleable.ZebraSearch_textFieldStyle, R.style.Widget_Zebra_SearchField);
    mLeadingDrawable = attributes.getDrawable(R.styleable.ZebraSearch_leadingDrawable);
    if (mLeadingDrawable != null) {
      mLeadingDrawable.setBounds( 0, 0, px(24), px(24));
      TypedArray colorAttributes = getContext().getTheme().obtainStyledAttributes(new int[] {
          R.attr.textColorSecondary
      });
      mLeadingDrawable.setTint(colorAttributes.getColor(0, -1));
    }


    mTextField = new EditText(new ContextThemeWrapper(getContext(), textFieldStyles), null, textFieldStyles);
    mTextField.setCompoundDrawablePadding(px(8));
    mEndPaddng = mTextField.getPaddingEnd();
    mTextField.setOnTouchListener(this);
    mTextField.addTextChangedListener(this);
    mTextField.setCompoundDrawables(mLeadingDrawable, null, null, null);
    mTextField.setOnEditorActionListener(this);

    if (attributes.hasValue(R.styleable.ZebraSearch_placeholder)) {
      String placeholder = attributes.getString(R.styleable.ZebraSearch_placeholder);
      if (placeholder != null) {
        mTextField.setHint(placeholder);
      }
    }

    if (attributes.hasValue(R.styleable.ZebraSearch_android_text)) {
      String text = attributes.getString(R.styleable.ZebraSearch_android_text);
      if (text != null) {
        setText(text);
      }
    }

    LayoutParams lp = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1);
    addView(mTextField, lp);

    attributes.recycle();

    init();
  }

  public void setText(String text) {
    mTextField.setText(text);
  }

  public Editable getText() {
    return mTextField.getText();
  }

  private void init() {
    mPassThroughListener = new PassThroughHierarchyChangeListener();
    super.setOnHierarchyChangeListener(mPassThroughListener);
  }

  public void setOnSubmitListener(OnSubmitListener listener) {
    mOnSubmitListener = listener;
  }

  public void setOnChangeListener(OnChangeListener listener) {
    mOnChangeListener = listener;
  }

  public void setOnFocusChangeListener(OnFocusChangeListener listener) {
    mOnFocusChangeListener = listener;
    mTextField.setOnFocusChangeListener(listener);
  }

  private int px(int size) {
    return Utils.px(size, getContext());
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    if(event.getAction() == MotionEvent.ACTION_UP && mLeadingDrawable != null)
    {
      Rect rBounds = mLeadingDrawable.getBounds();
      final int x = (int)event.getX();
      final int y = (int)event.getY();

      if(x>=v.getPaddingLeft() && x<=(v.getLeft()+rBounds.width())
          && y>=v.getPaddingTop() && y<=(v.getHeight()-v.getPaddingBottom()))
      {
        if (mTextField.isFocused()) {
          Log.i("ZebraSearch", "onSubmit");
          if (mOnSubmitListener != null) {
            mOnSubmitListener.onSubmit(v);
          }
          event.setAction(MotionEvent.ACTION_CANCEL);
        }
      }
    }
    return false;
  }

  @Override
  public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
      if (mOnSubmitListener != null) {
        mOnSubmitListener.onSubmit(v);
      }
      return true;
    }
    return false;
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    if (mOnChangeListener != null) {
      mOnChangeListener.onChange(mTextField.getText().toString());
    }
  }

  @Override
  public void afterTextChanged(Editable s) {}

  public interface OnSubmitListener {
    void onSubmit(View v);
  }

  public interface OnChangeListener {
    void onChange(String value);
  }

  private class PassThroughHierarchyChangeListener implements
      ViewGroup.OnHierarchyChangeListener {
    private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;

    @Override
    public void onChildViewAdded(View parent, View child) {
      if (parent == ZebraSearch.this) {
        if (((ViewGroup)parent).getChildCount() >= 2) {
          mTextField.setPadding(
              mTextField.getPaddingStart(),
              mTextField.getPaddingTop(),
              0,
              mTextField.getPaddingBottom()
          );
        }
      }

      if (mOnHierarchyChangeListener != null) {
        mOnHierarchyChangeListener.onChildViewAdded(parent, child);
      }
    }

    @Override
    public void onChildViewRemoved(View parent, View child) {
      if (parent == ZebraSearch.this) {
        if (((ViewGroup)parent).getChildCount() == 1) {
          mTextField.setPadding(
              mTextField.getPaddingStart(),
              mTextField.getPaddingTop(),
              mEndPaddng,
              mTextField.getPaddingBottom()
          );
        }
      }

      if (mOnHierarchyChangeListener != null) {
        mOnHierarchyChangeListener.onChildViewRemoved(parent, child);
      }
    }
  }
}
