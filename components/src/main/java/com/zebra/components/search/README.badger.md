# Search with leading icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    app:leadingDrawable="@drawable/search" />
```

# Search with leading and trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    app:leadingDrawable="@drawable/search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/microphone" />
</com.zebra.Zebra.search.ZebraSearch>
```

# Search with leading icon and placeholder

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    app:placeholder="Search"
    app:leadingDrawable="@drawable/search" />
```

# Search with leading and trailing icon and placeholder

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    app:placeholder="Search"
    app:leadingDrawable="@drawable/search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/microphone" />
</com.zebra.Zebra.search.ZebraSearch>
```

# Search with trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp" >

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/search" />
</com.zebra.Zebra.search.ZebraSearch>
```

# Search with trailing icon and placeholder

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    app:placeholder="Search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/search" />
</com.zebra.Zebra.search.ZebraSearch>
```

# Search with trailing icon prepopulated value and search suggestions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:id="@+id/search_field"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="8dp"
    android:text="Pixel 3 phones"
    app:placeholder="Search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/search" />
</com.zebra.Zebra.search.ZebraSearch>
```

```java
final ZebraSearch search = view.findViewById(R.id.search_field);
final ZebraMenuPopup menu = new ZebraMenuPopup(getContext());

final Bundle options = new Bundle();
options.putBoolean("focusable", false);
options.putBoolean("outsideTouchable", false);
options.putSize("offset", new Size(px(-5), 0));

ArrayList<MenuItem> items = new ArrayList<>();

items.add(new MenuItem(0, "Show all notification lorem ipsum dolor sit amet"));
items.add(new MenuItem(1, "Hide sensitive lorem ipsum dolor sit amet"));
items.add(new MenuItem(2, "Don’t show notifications at all"));

menu.setItems(items);

final Drawable cardHeader = ContextCompat.getDrawable(getContext(), R.drawable.card_header);

search.setOnChangeListener(new ZebraSearch.OnChangeListener() {
    @Override
    public void onChange(String value) {
        if (value.length() >= 3) {
            search.setForeground(cardHeader);
            menu.show(search, options);
        }else {
            search.setForeground(null);
            menu.dismiss();
        }
    }
});

search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (search.getText().toString().length() >= 3) {
                search.setForeground(cardHeader);
                menu.show(search, options);
            }
        }else {
            search.setForeground(null);
            menu.dismiss();
        }
    }
});
```
