
# Mobile Search Example with placeholder and leading icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed! 

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="8dp"
    app:leadingDrawable="@drawable/search"
    app:placeholder="Search" />
```

# Mobile Search Example with trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed! 
 
```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="8dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="8dp"
    app:placeholder="Search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/search" />
</com.zebra.Zebra.search.ZebraSearch>
```

# Mobile Search Example with leading, trailing icon and placeholder 

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed! 

```xml
<com.zebra.Zebra.search.ZebraSearch
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginStart="8dp"
    android:layout_marginTop="8dp"
    android:layout_marginEnd="8dp"
    android:layout_marginBottom="8dp"
    app:leadingDrawable="@drawable/search"
    app:placeholder="Search">

    <com.zebra.Zebra.ripple.ZebraRipple
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/microphone" />
</com.zebra.Zebra.search.ZebraSearch>
```