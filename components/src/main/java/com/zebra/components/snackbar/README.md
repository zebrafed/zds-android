# Snackbars
Snackbars provide brief messages about app processes at the bottom of the screen.

# Snackbar without acton example
Snackbar should be used inside of a coordinator layout viewgroup do be best.
```java
ZebraSnackbar sb1 = ZebraSnackbar.make(viewGroup, ZebraSnackbar.LENGTH_SHORT);
sb1.setText("Snackbar message here. Ideally short enough to fit into a single line.");
sb1.show();
```

# Snackbar with action example
```java
ZebraSnackbar sb2 = ZebraSnackbar.make(viewGroup, ZebraSnackbar.LENGTH_LONG);
sb2.setText("Snackbar message here. Ideally short enough to fit into a single line.");
sb2.setAction("ACTION", new View.OnClickListener() {
    @Override
     public void onClick(View v) {
        //Do something
     }
});
sb2.show();
```

# Snackbar with urgent action example
```java
ZebraSnackbar sb3 = ZebraSnackbar.make(viewGroup, ZebraSnackbar.LENGTH_INDEFINITE);
sb3.setText("Snackbar message here. Ideally short enough to fit into a single line.");
sb3.setAction("ACTION", new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        //do something
    }
});
sb3.setBackgroundColor(getContext().getColor(R.color.red));
sb3.setTextColor(getContext().getColor(R.color.white));
sb3.setButtonTextColor(getContext().getColor(R.color.white));
sb3.show();
```