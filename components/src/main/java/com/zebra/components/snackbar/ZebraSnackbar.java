package com.zebra.components.snackbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.zebra.components.R;
import com.zebra.components.button.ZebraButton;

import static com.zebra.components.Utils.Mini;
import static com.zebra.components.Utils.px;

public class ZebraSnackbar extends BaseTransientBottomBar<ZebraSnackbar> {

    private ZebraSnackbar(ViewGroup parent, View content, ContentViewCallback contentViewCallback) {
        super(parent, content, contentViewCallback);
    }

    private static class ContentViewCallback implements
            BaseTransientBottomBar.ContentViewCallback {
        private View content;

        public ContentViewCallback(View content) {
            this.content = content;
        }

        @Override
        public void animateContentIn(int i, int i1) {

        }

        @Override
        public void animateContentOut(int i, int i1) {

        }
    }

    public static ZebraSnackbar make(ViewGroup parent, @Duration int duration) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View content = inflater.inflate(R.layout.snackbar, parent, false);
        ContentViewCallback callback = new ContentViewCallback(content);
        ZebraSnackbar sb = new ZebraSnackbar(parent, content, callback);
        sb.setDuration(duration);
        return sb;
    }

    public ZebraSnackbar setText(CharSequence text) {
        int padding = px(16, getContext());
        if (Mini(getContext())) {
            padding = px(8, getContext());
        }

        TextView textView = getView().findViewById(R.id.snackbar_text);
        textView.setText(text);
        if (Mini(getContext())) {
            LinearLayout linearLayout = getView().findViewById(R.id.linearLayout);
            linearLayout.setPadding(padding, 0, padding, 0);
            textView.setMaxLines(4);
        }
        return this;
    }

    public ZebraSnackbar setAction(CharSequence text, final View.OnClickListener listener) {
        ZebraButton actionView = getView().findViewById(R.id.snackbar_action);
        actionView.setText(text);
        actionView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.roboto_medium));
        actionView.setVisibility(View.VISIBLE);
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view);
                dismiss();
            }
        });
        return this;
    }

    public void setBackgroundColor(int color) {
        LinearLayout linearLayout = view.findViewById(R.id.linearLayout);
        linearLayout.setBackgroundColor(color);
        ZebraButton actionView = getView().findViewById(R.id.snackbar_action);
        actionView.setBackgroundColor(color);
    }

    public void setTextColor(int color) {
        TextView textView = getView().findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
    }

    public void setButtonTextColor(int color) {
        ZebraButton actionView = getView().findViewById(R.id.snackbar_action);
        actionView.setTextColor(color);
    }
}