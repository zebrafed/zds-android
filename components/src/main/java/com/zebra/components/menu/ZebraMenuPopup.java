package com.zebra.components.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Size;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;

import com.zebra.components.R;
import com.zebra.components.Utils;
import com.zebra.components.dropdown.OnItemClickListener;

import java.util.ArrayList;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ZebraMenuPopup implements AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

  private final int MAX_HEIGHT = 165;
  private final int MARGIN = 5;
  private final Context mContext;
  private final FrameLayout mDropdownLayout;
  private final ListView mList;
  private final MenuAdapter mAdapter;

  private OnItemClickListener mOnItemClickListener;
  private PopupWindow.OnDismissListener mOnDismissListener;
  private PopupWindow mDropdown;
  private boolean mIsVisible;

  public ZebraMenuPopup(@NonNull Context context) {
    mContext = context;

    mDropdownLayout = new FrameLayout(context);
    mDropdownLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

    int margin = px(MARGIN);

    mList = new ListView(context);
    mList.setBackgroundResource(R.color.white);
    mList.setElevation(10);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
    params.setMargins(margin, 0, margin, margin);

    ArrayList<MenuItem> data = new ArrayList<>();

    data.add(new MenuItem(0, "Menu item"));
    data.add(new MenuItem(1, "Menu item"));
    data.add(new MenuItem(2, "Menu item"));

    mAdapter = new MenuAdapter(getContext(), data);

    mList.setOnItemClickListener(this);

    mList.setPadding(0,0,0,0);
    mList.setAdapter(mAdapter);
    mList.setDivider(null);
    mList.setDividerHeight(0);

    mDropdownLayout.addView(mList, params);
  }

  public void addItem(MenuItem item) {
    mAdapter.add(item);
  }

  public void addItems(ArrayList<MenuItem> items) {
    mAdapter.addAll(items);
  }

  public void setItems(ArrayList<MenuItem> items) {
    mAdapter.clear();
    mAdapter.addAll(items);
  }

  public void show(@NonNull View anchor) {
    Bundle options = new Bundle();
    options.putBoolean("focusable", true);
    options.putBoolean("outsideTouchable", true);
    options.putSize("offset", new Size(px(MARGIN), 0));

    show(anchor, options);
  }

  public void show(@NonNull View anchor, Bundle options) {
    if (mIsVisible) {
      return;
    }

    boolean outsideTouchable = options.getBoolean("outsideTouchable", true);
    boolean focusable = options.getBoolean("focusable", true);
    Size offset = options.getSize("offset");
    int margin = px(MARGIN);
    anchor.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

    mDropdown = new PopupWindow(mDropdownLayout, anchor.getWidth() + margin + margin, px(MAX_HEIGHT));
    mDropdown.setOutsideTouchable(outsideTouchable);
    mDropdown.setFocusable(focusable);
    mDropdown.setOnDismissListener(this);

    mIsVisible = true;

    mDropdown.showAsDropDown(anchor, offset.getWidth(), offset.getHeight(), Gravity.START);
  }

  private Resources getResources() {
    return getContext().getResources();
  }

  private int px(int value) {
    return Utils.px(value, getContext());
  }

  private Context getContext() {
    return mContext;
  }

  public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
    this.mOnDismissListener = onDismissListener;
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    this.mOnItemClickListener = onItemClickListener;
  }

  public boolean isVisible() {
    return mIsVisible;
  }

  public void dismiss() {
    if (mDropdown != null) {
      mDropdown.dismiss();
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    mDropdown.dismiss();
    if (mOnDismissListener != null) {
      mOnDismissListener.onDismiss();
    }
    if (mOnItemClickListener != null) {
      mOnItemClickListener.onItemClick(view, position, id, mAdapter.getItem(position));
    }
  }

  @Override
  public void onDismiss() {
    mIsVisible = false;
    if (mOnDismissListener != null) {
      mOnDismissListener.onDismiss();
    }
  }

  private class MenuAdapter extends ArrayAdapter<MenuItem> {

    public MenuAdapter(@NonNull Context context, ArrayList<MenuItem> items) {
      super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      MenuItem item = getItem(position);

      if (convertView == null) {
        convertView = new ZebraMenuItem(getContext());
      }

      ((ZebraMenuItem) convertView).setText(item.getText());

      return convertView;
    }

  }
}
