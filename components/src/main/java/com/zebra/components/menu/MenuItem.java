package com.zebra.components.menu;

public class MenuItem {
  private int mId;
  private String mText;

  public MenuItem(int mId, String mText) {
    this.mId = mId;
    this.mText = mText;
  }

  public int getId() {
    return mId;
  }

  public String getText() {
    return mText;
  }
}
