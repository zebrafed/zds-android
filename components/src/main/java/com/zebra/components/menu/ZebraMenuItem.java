package com.zebra.components.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zebra.components.R;

import static com.zebra.components.Utils.createThemedContext;
import static com.zebra.components.Utils.obtainStyledAttributes;

public class ZebraMenuItem extends LinearLayout {
  private static final int DEF_STYLE_RES = R.style.Widget_Mini_MenuItem;
  private final TextView mText;

  public ZebraMenuItem(Context context) {
    this(context, null);
  }

  public ZebraMenuItem(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, R.attr.ZebraMenuItem);
  }

  public ZebraMenuItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);

    TypedArray attributes = obtainStyledAttributes(getContext(), attrs, R.styleable.ZebraMenuItem, defStyleAttr, DEF_STYLE_RES);

    int mTextAppearance = attributes.getResourceId(R.styleable.ZebraMenuItem_textAppearance, R.style.Widget_Zebra_MenuItem_TextAppearance);
    int mMinHeight = attributes.getDimensionPixelSize(R.styleable.ZebraMenuItem_android_minHeight, 0);
    int mPt = attributes.getDimensionPixelSize(R.styleable.ZebraMenuItem_android_paddingTop, 0);
    int mPb = attributes.getDimensionPixelSize(R.styleable.ZebraMenuItem_android_paddingBottom, 0);
    int mPl = attributes.getDimensionPixelSize(R.styleable.ZebraMenuItem_android_paddingLeft, 0);
    int mPr = attributes.getDimensionPixelSize(R.styleable.ZebraMenuItem_android_paddingRight, 0);

    mText = new TextView(getContext());
    mText.setTextAppearance(getContext(), mTextAppearance);

    setMinimumHeight(mMinHeight);
    setPadding(mPl, mPt, mPr, mPb);

    addView(mText);
  }

  public void setText(String text) {
    mText.setText(text);
  }
}
