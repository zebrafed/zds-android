# Zebra design system: Android components

These are components. This readme needs to be written properly by someone Mike.

# How to use the components

1. Add to app-level build.gradle

```
dependencies {
    implementation: 'com.zebra.components'
}
```

2. App theme must inherit from a Zebra theme:

    *  For mobile devices:
    ```xml
    <style name="AppTheme" parent="Theme.ZebraMobile">
    ```
    
    *  For mini devices:
    ```xml
    <style name="AppTheme" parent="Theme.ZebraMini">
    ```
3. Use the components :D