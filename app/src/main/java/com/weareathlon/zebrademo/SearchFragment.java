package com.weareathlon.zebrademo;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.zebra.components.Utils;
import com.zebra.components.menu.ZebraMenuPopup;
import com.zebra.components.menu.MenuItem;
import com.zebra.components.search.ZebraSearch;

import java.util.ArrayList;

public class SearchFragment extends Fragment {

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.content_search, null);

    final ZebraSearch search = view.findViewById(R.id.search_field);
    final ZebraMenuPopup menu = new ZebraMenuPopup(getContext());

    final Bundle options = new Bundle();
    options.putBoolean("focusable", false);
    options.putBoolean("outsideTouchable", false);
    options.putSize("offset", new Size(px(-5), 0));

    ArrayList<MenuItem> items = new ArrayList<>();

    items.add(new MenuItem(0, "Show all notification lorem ipsum dolor sit amet"));
    items.add(new MenuItem(1, "Hide sensitive lorem ipsum dolor sit amet"));
    items.add(new MenuItem(2, "Don’t show notifications at all"));

    menu.setItems(items);

    final Drawable cardHeader = ContextCompat.getDrawable(getContext(), R.drawable.card_header);

    search.setOnChangeListener(new ZebraSearch.OnChangeListener() {
      @RequiresApi(api = Build.VERSION_CODES.M)
      @Override
      public void onChange(String value) {
        if (value.length() >= 3) {
          search.setForeground(cardHeader);
          menu.show(search, options);
        }else {
          search.setForeground(null);
          menu.dismiss();
        }
      }
    });

    search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @RequiresApi(api = Build.VERSION_CODES.M)
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
          if (search.getText().toString().length() >= 3) {
            search.setForeground(cardHeader);
            menu.show(search, options);
          }
        }else {
          search.setForeground(null);
          menu.dismiss();
        }
      }
    });

    if (search.getText().toString().length() >= 3) {
      menu.show(search, options);
    }

    return view;
  }

  private int px(int value) {
    return Utils.px(value, getContext());
  }
}
