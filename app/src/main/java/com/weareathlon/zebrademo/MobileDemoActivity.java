package com.weareathlon.zebrademo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.zebra.components.dialog.ZebraDialog;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MobileDemoActivity extends AppCompatActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_mobile_demo);

    simpleDialog();
    simpleTitleDialog();
  }

  private void simpleDialog() {
    final ZebraDialog dialog = new ZebraDialog(this, R.layout.dialog_mobile_simple);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = findViewById(R.id.dialog_button2);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }

  private void simpleTitleDialog() {
    final ZebraDialog dialog = new ZebraDialog(this, R.layout.dialog_mobile_title_simple);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = findViewById(R.id.dialog_button1);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }

}
