package com.weareathlon.zebrademo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.zebra.components.floatingactionbutton.ZebraFloatingActionButton;
import com.zebra.components.snackbar.ZebraSnackbar;

public class ButtonFragment extends Fragment {
private static final String TAG = "ButtonFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.content_button, null);

        view.findViewById(R.id.fab1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb1 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_SHORT);
                sb1.setText("Snackbar message here. Ideally short enough to fit into a single line.");
                sb1.show();
            }
        });

        view.findViewById(R.id.fab2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb2 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_LONG);
                sb2.setText("Snackbar message here. Ideally short enough to fit into a single line.");
                sb2.setAction("ACTION", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do something
                    }
                });
                sb2.show();
            }
        });
        return view;
    }


}
