package com.weareathlon.zebrademo;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zebra.components.button.ZebraButton;
import com.zebra.components.timepicker.ZebraTimePicker;



public class page1 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_page1, null);

        ZebraButton button = view.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setPositiveButton("OK", null);
                builder.setNegativeButton("Cancel", null);
                AlertDialog d = builder.create();
                ZebraTimePicker ztp = new ZebraTimePicker((getContext()));
                ztp.setHour(20);
                ztp.setMinute(30);
                d.setView(ztp);
                d.show();
            }
        });
        return view;
    }
}
