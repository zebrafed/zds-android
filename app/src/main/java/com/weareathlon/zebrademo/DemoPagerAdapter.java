package com.weareathlon.zebrademo;

import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class DemoPagerAdapter extends FragmentPagerAdapter {
    public DemoPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    private final Fragment[] fragments = {
            new CardsFragment(),
            new DropdownsFragment(),
            new SwitchesFragment(),
            new SearchFragment(),
            new DialogFragment(),
            new ButtonFragment(),
            new InfoFragment(),
            new InputFragment()
    };

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
