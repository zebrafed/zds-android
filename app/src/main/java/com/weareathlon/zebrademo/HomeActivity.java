package com.weareathlon.zebrademo;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.zebra.components.header.ZebraHeader;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private ViewPager mPager;
    private ZebraHeader toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        DemoPagerAdapter mViewPageAdapter = new DemoPagerAdapter(getSupportFragmentManager());

        mPager = findViewById(R.id.pv);
        mPager.addOnPageChangeListener(this);
        mPager.setAdapter(mViewPageAdapter);

        getSupportActionBar().setTitle("Page title");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_card) {
            mPager.setCurrentItem(0);
        } else if (id == R.id.nav_dropdown) {
            mPager.setCurrentItem(1);
        } else if (id == R.id.nav_switches) {
            mPager.setCurrentItem(2);
        } else if (id == R.id.nav_search) {
            mPager.setCurrentItem(3);
        } else if (id == R.id.nav_dialog) {
            mPager.setCurrentItem(4);
        } else if (id == R.id.nav_button) {
            mPager.setCurrentItem(5);
        } else if (id == R.id.nav_input) {
            mPager.setCurrentItem(7);
        } else if (id == R.id.nav_info) {
            mPager.setCurrentItem(6);
        } else if (id == R.id.nav_drawer) {
            Intent intent = new Intent(this, DrawerDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_portable_drawer) {
            Intent intent = new Intent(this, SmallDrawerDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mobile) {
            Intent intent = new Intent(this, MobileDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mobile_drawer) {
            Intent intent = new Intent(this, MobileDrawerDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mobile_small_drawer) {
            Intent intent = new Intent(this, MobileSmallDrawerDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mobile_large_drawer) {
            Intent intent = new Intent(this, MobileLargeDrawerDemoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_another_one) {
            Intent intent = new Intent(this, AnotherActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        NavigationView navigationView = findViewById(R.id.nav_view);
        //onNavigationItemSelected(navigationView.getMenu().getItem(position));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
