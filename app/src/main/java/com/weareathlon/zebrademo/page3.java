package com.weareathlon.zebrademo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zebra.components.snackbar.ZebraSnackbar;

public class page3 extends Fragment {
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_page3, null);

        view.findViewById(R.id.snackbar1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb1 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_SHORT);
                sb1.setText("Snackbar message here. Ideally short enough to fit into a single line.");
                sb1.show();
            }
        });

        view.findViewById(R.id.snackbar2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb2 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_LONG);
                sb2.setText("Snackbar message here. Ideally short enough to fit into a single line.");
                sb2.setAction("ACTION", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do something
                    }
                });
                sb2.show();
            }
        });

        view.findViewById(R.id.snackbar3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb3 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_INDEFINITE);
                sb3.setText("Snackbar message here. Ideally short enough to fit into a single line.");
                sb3.setAction("ACTION", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do something
                    }
                });
                sb3.setBackgroundColor(getContext().getColor(R.color.red));
                sb3.setTextColor(getContext().getColor(R.color.white));
                sb3.setButtonTextColor(getContext().getColor(R.color.white));
                sb3.show();
            }
        });

        view.findViewById(R.id.snackbar4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZebraSnackbar sb4 = ZebraSnackbar.make((ViewGroup) view.findViewById(R.id.coordinator), ZebraSnackbar.LENGTH_INDEFINITE);
                sb4.setText("Fits into a single line.");
                sb4.setButtonTextColor(getContext().getColor(R.color.green));
                sb4.setAction("ACTION", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do something
                    }
                });

                sb4.show();
            }
        });

        return view;
    }
}
