package com.weareathlon.zebrademo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class AnotherPagerAdapter extends FragmentPagerAdapter {
    public AnotherPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new page1();
            case 1:
                return new page2();
            case 2:
                return new page3();
            case 3:
                return new page4();
            case 4:
                return new page5();
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 5; //5 fragments
    }
}
