package com.weareathlon.zebrademo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zebra.components.action.ZebraAction;
import com.zebra.components.timepicker.ZebraTimePicker;

public class page5 extends Fragment {
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_page5, null);

        final ZebraTimePicker ztp = view.findViewById(R.id.timepicker);
        ztp.setHour(14);
        ztp.setMinute(57);


        ZebraAction cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ztp.setEnabled(false);
            }
        });

        ZebraAction ok = view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ztp.setEnabled(true);
            }
        });
        return view;
    }
}
