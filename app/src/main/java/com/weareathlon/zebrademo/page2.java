package com.weareathlon.zebrademo;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.zebra.components.button.ZebraButton;
import com.zebra.components.button.ZebraPageButton;
import com.zebra.components.floatingactionbutton.ZebraFloatingActionButton;
import com.zebra.components.input.ZebraInput;
import com.zebra.components.input.ZebraTextInputLayout;

public class page2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_page2, null);
        ZebraTextInputLayout l = view.findViewById(R.id.zebraTextInputLayout);

        ZebraTextInputLayout highlight = view.findViewById(R.id.input_highlighted);
//        highlight.requestFocus();

        ZebraTextInputLayout error = view.findViewById(R.id.textinput_error);
        error.setErrorType(ZebraTextInputLayout.EMAIL);

        ZebraTextInputLayout errorphone = view.findViewById(R.id.textinput_error_phone);
        errorphone.setErrorType(ZebraTextInputLayout.PHONE_NUMBER);

        return view;
    }
}
