package com.weareathlon.zebrademo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zebra.components.bottomnav.ZebraBottomNavigation;
import com.zebra.components.header.ZebraHeader;

public class AnotherActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private static final String TAG = "com.weareathlon.zebrademo.AnotherActivity";
    private ViewPager mPager;
    private ZebraBottomNavigation mBottom;
    private MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);
        ZebraHeader toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setOverflowIcon(getDrawable(R.drawable.grid3x3));

        toolbar.setTitle("Page title");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBottom = findViewById(R.id.bottom);
        AnotherPagerAdapter mViewPageAdapter = new AnotherPagerAdapter(getSupportFragmentManager());
        mPager = findViewById(R.id.view_pager);
        mPager.addOnPageChangeListener(this);
        mPager.setAdapter(mViewPageAdapter);

        mBottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.page1:
                        mPager.setCurrentItem(0);
                        break;
                    case R.id.page2:
                        mPager.setCurrentItem(1);
                        break;
                    case R.id.page3:
                        mPager.setCurrentItem(2);
                        break;
                    case R.id.page4:
                        mPager.setCurrentItem(3);
                        break;
                    case R.id.page5:
                        mPager.setCurrentItem(4);
                        break;
                }
                return false;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }



    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageSelected(int position) {
        if (prevMenuItem != null)
            prevMenuItem.setChecked(false);
        else
            mBottom.getMenu().getItem(0).setChecked(false);

        mBottom.getMenu().getItem(position).setChecked(true);
        prevMenuItem = mBottom.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
