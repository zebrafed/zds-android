package com.weareathlon.zebrademo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zebra.components.Utils;
import com.zebra.components.dialog.ZebraBottomSheet;
import com.zebra.components.dialog.ZebraDialog;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class DialogFragment extends Fragment {
  static private final int WIDTH = 240;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.content_dialog, null);

    simpleDialog(view);

    simpleThreeActionsDialog(view);

    mediDialog(view);

    actionsDialog(view);

    return view;
  }

  private void actionsDialog(View view) {
    final ZebraBottomSheet dialog = new ZebraBottomSheet(getContext(), R.layout.dialog_simple_actions);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        Button okOnce = dialog.findViewById(R.id.once);
        okOnce.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button okAlways = dialog.findViewById(R.id.always);
        okAlways.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = view.findViewById(R.id.button4);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }

  private void mediDialog(View view) {
    final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog_simple_media);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(px(WIDTH), WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.always);
        okButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = view.findViewById(R.id.button3);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }

  private void simpleThreeActionsDialog(View view) {
    final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog_simple_three);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(px(WIDTH), WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button defaultButton = dialog.findViewById(R.id.button_default);
        defaultButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = view.findViewById(R.id.button2);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }


  private void simpleDialog(View view) {
    final ZebraDialog dialog = new ZebraDialog(getContext(), R.layout.dialog_simple);

    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface iDialog) {
        dialog.getWindow().setLayout(px(WIDTH), WRAP_CONTENT);

        Button okButton = dialog.findViewById(R.id.once);
        okButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });

        Button cancelButton = dialog.findViewById(R.id.always);
        cancelButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
          }
        });
      }
    });

    final Button button = view.findViewById(R.id.button1);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.show();
      }
    });
  }

  private int px(int value) {
    return Utils.px(value, getContext());
  }
}
