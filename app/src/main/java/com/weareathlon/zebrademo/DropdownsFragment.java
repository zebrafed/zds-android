package com.weareathlon.zebrademo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zebra.components.card.ZebraCardHeaderView;
import com.zebra.components.dropdown.ZebraDropdown;
import com.zebra.components.dropdown.OnItemClickListener;
import com.zebra.components.menu.MenuItem;

public class DropdownsFragment extends Fragment {

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.content_dropdown, null);

    ZebraDropdown dropdown = view.findViewById(R.id.custom_dropdown);
    final ZebraCardHeaderView header = view.findViewById(R.id.custom_dropdown_header);

    dropdown.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(View view, int position, long id, MenuItem item) {
        header.setSubHeader(item.getText());
      }
    });

    return view;
  }
}
